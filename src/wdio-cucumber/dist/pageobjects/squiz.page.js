'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _chai = require('chai');

var _page = require('./page');

var _page2 = _interopRequireDefault(_page);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var config = require('config');

var squizPage = function (_Page) {
    _inherits(squizPage, _Page);

    function squizPage() {
        _classCallCheck(this, squizPage);

        return _possibleConstructorReturn(this, (squizPage.__proto__ || Object.getPrototypeOf(squizPage)).apply(this, arguments));
    }

    _createClass(squizPage, [{
        key: 'loginToSquiz',
        value: function loginToSquiz(username, password) {
            this.squizUserName.waitForVisible();
            this.squizUserName.clearElement();
            this.squizUserName.setValue(username);
            this.squizPassword.waitForVisible();
            this.squizPassword.clearElement();
            this.squizPassword.setValue(password);
            this.squizLoginButton.click();
            this.squizLogoutButton.waitForVisible(4500);
            return (0, _chai.expect)(this.squizLogoutButton.isVisible()).to.be.eql(true);
        }
    }, {
        key: 'selectOption',
        value: function selectOption(optionName) {
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = this.squizLeftSideOptions[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var obj = _step.value;

                    if (obj.getText() === optionName) {
                        return obj.click();
                    }
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }
        }
    }, {
        key: 'verifyText',
        value: function verifyText(itemText) {

            var itemTexts = [];
            var _iteratorNormalCompletion2 = true;
            var _didIteratorError2 = false;
            var _iteratorError2 = undefined;

            try {
                for (var _iterator2 = this.squizNewItemFields[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                    var obj = _step2.value;

                    itemTexts.push(obj.getText());
                }
            } catch (err) {
                _didIteratorError2 = true;
                _iteratorError2 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion2 && _iterator2.return) {
                        _iterator2.return();
                    }
                } finally {
                    if (_didIteratorError2) {
                        throw _iteratorError2;
                    }
                }
            }

            return (0, _chai.expect)(itemTexts).to.include.members(itemText);
        }
    }, {
        key: 'changeViewToIosMobile',
        value: function changeViewToIosMobile() {
            return browser.execute('$("html").removeClass("picture csscalc flexbox ddDesktop ddWeb").addClass("picture csscalc flexbox ddIos ddWeb");');
        }
    }, {
        key: 'changeViewToAndroidMobile',
        value: function changeViewToAndroidMobile() {
            return browser.execute('$("html").removeClass("picture csscalc flexbox ddDesktop ddWeb").addClass("picture csscalc flexbox ddAndroid ddWeb");');
        }
    }, {
        key: 'scroll',
        value: function scroll() {
            return browser.execute('$(".form-control.ng-pristine.ng-untouched.ng-valid.ng-scope.ng-not-empty").get(2).scrollIntoView();');
        }
    }, {
        key: 'scrollToLinkedGame',
        value: function scrollToLinkedGame() {
            return browser.execute('$(".form-control.ng-pristine.ng-untouched.ng-valid.ng-scope.ng-not-empty").get(2).scrollIntoView();');
        }
    }, {
        key: 'scrollToLogout',
        value: function scrollToLogout() {
            return browser.execute('$("html, body").scrollTop(0);');
        }
    }, {
        key: 'scrollToCreateNotification',
        value: function scrollToCreateNotification() {
            return browser.execute('$(".btn.btn-primary.btn-xl.ng-scope").scrollTop(0);');
        }
    }, {
        key: 'createNotification',
        value: function createNotification(notificationType, notificationName) {
            browser.pause(6000);
            //Static wait as currently there is an issue with data being wiped out after sometimes
            this.notificationTitle.waitForVisible();
            this.notificationTitle.setValue(notificationName);
            this.notificationBodyTxt.setValue("This is a test notification");
            this.notificationTypeDropDown.click();
            this.notificationTypeMenu[0].waitForVisible();
            this.scroll();
            var _iteratorNormalCompletion3 = true;
            var _didIteratorError3 = false;
            var _iteratorError3 = undefined;

            try {
                for (var _iterator3 = this.notificationTypeMenu[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                    var menu = _step3.value;


                    menu.getText() === notificationType ? menu.click() : '';
                }
            } catch (err) {
                _didIteratorError3 = true;
                _iteratorError3 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion3 && _iterator3.return) {
                        _iterator3.return();
                    }
                } finally {
                    if (_didIteratorError3) {
                        throw _iteratorError3;
                    }
                }
            }

            browser.pause(1000);
            this.saveNotificaionBtn.waitForEnabled();
            this.saveNotificaionBtn.click();
        }
    }, {
        key: 'createGameNtfnWithLink',
        value: function createGameNtfnWithLink() {
            browser.pause(6000);
            //Static wait as currently there is an issue with data being wiped out after sometimes
            this.notificationTitle.waitForVisible();
            this.notificationTitle.setValue("Test Notification " + Math.random().toString(36));
            this.notificationBodyTxt.setValue("This is a test notification");
            this.notificationTypeDropDown.click();
            this.notificationTypeMenu[0].waitForVisible();
            this.scroll();
            var _iteratorNormalCompletion4 = true;
            var _didIteratorError4 = false;
            var _iteratorError4 = undefined;

            try {
                for (var _iterator4 = this.notificationTypeMenu[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                    var menu = _step4.value;


                    menu.getText() === 'Game' ? menu.click() : '';
                }
            } catch (err) {
                _didIteratorError4 = true;
                _iteratorError4 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion4 && _iterator4.return) {
                        _iterator4.return();
                    }
                } finally {
                    if (_didIteratorError4) {
                        throw _iteratorError4;
                    }
                }
            }

            this.scrollToLogout();
            this.notificationType2ndDropDown.waitForVisible();
            var _iteratorNormalCompletion5 = true;
            var _didIteratorError5 = false;
            var _iteratorError5 = undefined;

            try {
                for (var _iterator5 = this.notificationType2ndMenu[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
                    var _menu = _step5.value;


                    _menu.isVisible() ? _menu.click() : '';
                    break;
                }
            } catch (err) {
                _didIteratorError5 = true;
                _iteratorError5 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion5 && _iterator5.return) {
                        _iterator5.return();
                    }
                } finally {
                    if (_didIteratorError5) {
                        throw _iteratorError5;
                    }
                }
            }

            this.scroll();
            this.saveNotificaionBtn.waitForEnabled();
            this.saveNotificaionBtn.click();
        }
    }, {
        key: 'editNotification',
        value: function editNotification() {
            browser.pause(6000);
            //Static wait as currently there is an issue with data being wiped out after sometimes
            this.notificationTitle.waitForVisible();
            this.notificationBodyTxt.clearElement();
            this.notificationBodyTxt.setValue("This is an edited notification");
            this.saveNotificaionBtn.waitForEnabled();
            this.saveNotificaionBtn.click();
        }
    }, {
        key: 'goToCeiUrl',
        value: function goToCeiUrl(urlSelector) {
            var ceiUrl = void 0;
            if (!!urlSelector) {
                ceiUrl = config.get(process.env.npm_config_env + urlSelector);
            }
            browser.url(ceiUrl);
            return ceiUrl;
        }
    }, {
        key: 'rouletteIcon',
        get: function get() {
            return $('#xsell-roulette');
        }
    }, {
        key: 'squizUserName',
        get: function get() {
            return $('#SQ_LOGIN_USERNAME');
        }
    }, {
        key: 'squizPassword',
        get: function get() {
            return $('#SQ_LOGIN_PASSWORD');
        }
    }, {
        key: 'squizLoginButton',
        get: function get() {
            return $('#log_in_out_button');
        }
    }, {
        key: 'squizLogoutButton',
        get: function get() {
            return $('.logout-link.label.label-inverse');
        }
    }, {
        key: 'squizAddNewItemBtn',
        get: function get() {
            return $('.btn.btn-primary.btn-xl.ng-scope');
        }
    }, {
        key: 'squizAddNewNotificationBtn',
        get: function get() {
            return $('a[ui-sref="notificationsCreateNotification({ id: vm.parentId })"]');
        }
    }, {
        key: 'htmlClass',
        get: function get() {
            return $('html');
        }
    }, {
        key: 'notificationTitle',
        get: function get() {
            return $('#notification-title input');
        }
    }, {
        key: 'notificationBodyTxt',
        get: function get() {
            return $('#notification-body-text input');
        }
    }, {
        key: 'notificationTypeDropDown',
        get: function get() {
            return $('[ng-click="toggleDropdown()"]');
        }
    }, {
        key: 'notificationType2ndDropDown',
        get: function get() {
            return $('#wrapper > div > div > div.panel-body > form > div:nth-child(3) > div:nth-child(2) > div > div > div.ng-isolate-scope > div > button');
        }
    }, {
        key: 'saveNotificaionBtn',
        get: function get() {
            return $('button.btn.btn-primary');
        }
    }, {
        key: 'notificationActionMessage',
        get: function get() {
            return $('div.alert.ng-isolate-scope.alert-success.alert-dismissable');
        }
    }, {
        key: 'notificationTypeMenu',
        get: function get() {
            return $$('[ng-click="setSelectedItem(getPropertyForObject(option,settings.idProp))"]');
        }
    }, {
        key: 'notificationType2ndMenu',
        get: function get() {
            return $$('[ng-click="setSelectedItem(getPropertyForObject(option,settings.idProp))"]');
        }
    }, {
        key: 'squizNewItemFields',
        get: function get() {
            return $$('.input-group-addon.ng-binding');
        }
    }, {
        key: 'squizLeftSideOptions',
        get: function get() {
            return $$('.yes.text');
        }
    }, {
        key: 'iconCarouselTitles',
        get: function get() {
            return $$('.contextual-nav__title');
        }
    }, {
        key: 'notificationNames',
        get: function get() {
            return $$('div.main_container:nth-child(1) div.bs-docs div.row-fluid div tr td.custom-search__row.custom-search__row--text.ng-scope:nth-child(1) > div.custom-search__text-container.ng-binding');
        }
    }, {
        key: 'notificationEditButtons',
        get: function get() {
            return $$('[ui-sref="notificationsEditNotification({ id: row.id })"]');
        }
    }, {
        key: 'notificationDeleteButtons',
        get: function get() {
            return $$('[ng-click="vm.deleteNotification(row.id, row.title)"]');
        }
    }]);

    return squizPage;
}(_page2.default);

exports.default = new squizPage();
//# sourceMappingURL=squiz.page.js.map