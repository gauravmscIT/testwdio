'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _chai = require('chai');

var _page = require('./page');

var _page2 = _interopRequireDefault(_page);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var config = require('config');

var homePage = function (_Page) {
    _inherits(homePage, _Page);

    function homePage() {
        _classCallCheck(this, homePage);

        return _possibleConstructorReturn(this, (homePage.__proto__ || Object.getPrototypeOf(homePage)).apply(this, arguments));
    }

    _createClass(homePage, [{
        key: 'emailField',
        get: function get() {
            return $("[name='email'][class='hs-input']");
        }
    }, {
        key: 'subscribeBtn',
        get: function get() {
            return $("[value='Subscribe'][class='hs-button primary large']");
        }
    }, {
        key: 'confirmationMsg',
        get: function get() {
            return $(".submitted-message");
        }
    }, {
        key: 'sectionTitles',
        get: function get() {
            return $$(".mdl-typography--display-1.title");
        }
    }, {
        key: 'navigationLinks',
        get: function get() {
            return $$("a.mdl-navigation__link");
        }
    }]);

    return homePage;
}(_page2.default);

exports.default = new homePage();
//# sourceMappingURL=home.page.js.map