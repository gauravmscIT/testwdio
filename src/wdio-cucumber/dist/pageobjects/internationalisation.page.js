'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _chai = require('chai');

var _page = require('./page');

var _page2 = _interopRequireDefault(_page);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var config = require('config');

var internationalisationPage = function (_Page) {
    _inherits(internationalisationPage, _Page);

    function internationalisationPage() {
        _classCallCheck(this, internationalisationPage);

        return _possibleConstructorReturn(this, (internationalisationPage.__proto__ || Object.getPrototypeOf(internationalisationPage)).apply(this, arguments));
    }

    _createClass(internationalisationPage, [{
        key: 'buildSportsBookPageWithLang',
        value: function buildSportsBookPageWithLang(lang) {
            var endPoint = config.get(process.env.npm_config_env + '.ln_url');
            return endPoint + lang;
        }
    }, {
        key: 'buildSquizTGOWithLang',
        value: function buildSquizTGOWithLang(lang) {
            var endPoint = config.get(process.env.npm_config_env + '.squizTopGamesEndpoint');
            return endPoint.replace("en-gb", lang);
        }
    }, {
        key: 'buildNotificationEndpointWithLang',
        value: function buildNotificationEndpointWithLang(lang) {
            var endPoint = config.get(process.env.npm_config_env + '.squizNotificationJsonEndpoint');
            return endPoint.replace("en-gb", lang);
        }
    }, {
        key: 'buildGamesLobbyFragmentWithLang',
        value: function buildGamesLobbyFragmentWithLang(lang) {
            var endPoint = config.get(process.env.npm_config_env + '.gamesLobbyFragment');
            return endPoint.replace("en-gb", lang);
        }
    }]);

    return internationalisationPage;
}(_page2.default);

exports.default = new internationalisationPage();
//# sourceMappingURL=internationalisation.page.js.map