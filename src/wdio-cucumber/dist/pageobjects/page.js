'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _chai = require('chai');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Page = function Page() {
    _classCallCheck(this, Page);

    this.title = 'My Page';
};

exports.default = Page;
//# sourceMappingURL=page.js.map