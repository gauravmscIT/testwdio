'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _chai = require('chai');

var _page = require('./page');

var _page2 = _interopRequireDefault(_page);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var config = require('config');

var jackpotPage = function (_Page) {
    _inherits(jackpotPage, _Page);

    function jackpotPage() {
        _classCallCheck(this, jackpotPage);

        return _possibleConstructorReturn(this, (jackpotPage.__proto__ || Object.getPrototypeOf(jackpotPage)).apply(this, arguments));
    }

    _createClass(jackpotPage, [{
        key: 'mainJackpotComponent',
        get: function get() {
            return $(".millions.millions--desktop");
        }
    }, {
        key: 'mainTgoJackpotComponent',
        get: function get() {
            return $(".millions.millions--desktop");
        }
    }, {
        key: 'jackpotCarouselComponent',
        get: function get() {
            return $(".jackpots-carousel.jackpots-carousel .width-watcher .tiles-carousel.tiles-carousel--first-slide .carousel");
        }
    }, {
        key: 'jackpotCarouselNavArrowsComponent',
        get: function get() {
            return $(".jackpots-carousel.jackpots-carousel .width-watcher .tiles-carousel.tiles-carousel--first-slide .tiles-carousel__arrows.navigation-arrows");
        }
    }, {
        key: 'jackpotNextArrowBtn',
        get: function get() {
            return $(".wrapper.wrapper--desktop .navigation-arrows__arrow.navigation-arrows__arrow--next");
        }
    }, {
        key: 'jackpotPrevArrowBtn',
        get: function get() {
            return $(".wrapper.wrapper--desktop .navigation-arrows__arrow.navigation-arrows__arrow--prev");
        }
    }, {
        key: 'tgoJackpotCarouselComponent',
        get: function get() {
            return $(".carousel.carousel .width-watcher .tiles-carousel.tiles-carousel--first-slide .carousel");
        }
    }, {
        key: 'tgoJackpotCarouselNavArrowsComponent',
        get: function get() {
            return $(".carousel.carousel .width-watcher .tiles-carousel.tiles-carousel--first-slide .tiles-carousel__arrows.navigation-arrows");
        }
    }, {
        key: 'tgoJackpotNextArrowBtn',
        get: function get() {
            return $(".wrapper.wrapper--desktop .navigation-arrows__arrow.navigation-arrows__arrow--next");
        }
    }, {
        key: 'tgoJackpotPrevArrowBtn',
        get: function get() {
            return $(".wrapper.wrapper--desktop .navigation-arrows__arrow.navigation-arrows__arrow--prev");
        }
    }, {
        key: 'jackpotGamesTitles',
        get: function get() {
            return $$(".grid__jackpots.grid__category .tile-cover__title");
        }
    }, {
        key: 'tgoGamesTitles',
        get: function get() {
            return $$(".grid__jackpots.grid__category .tile-cover__title");
        }
    }, {
        key: 'jackpotTierNames',
        get: function get() {
            return $$(".tier__main .tier__content .tier__name");
        }
    }, {
        key: 'tgoTierNames',
        get: function get() {
            return $$("div.tier__main > div > div.tier__content > div.tier__name");
        }
    }, {
        key: 'jackpotTierAmts',
        get: function get() {
            return $$(".tier__main .tier__content .tier__amount");
        }
    }, {
        key: 'tgoTierAmts',
        get: function get() {
            return $$("div.tier__main > div > div.tier__content > div.tier__amount");
        }
    }, {
        key: 'jackpotNotificationItems',
        get: function get() {
            return $$(".notification__wrapper .notification__item");
        }
    }]);

    return jackpotPage;
}(_page2.default);

exports.default = new jackpotPage();
//# sourceMappingURL=jackpot.page.js.map