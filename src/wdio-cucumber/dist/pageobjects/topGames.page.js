'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _chai = require('chai');

var _page = require('./page');

var _page2 = _interopRequireDefault(_page);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var config = require('config');

var topGamesPage = function (_Page) {
    _inherits(topGamesPage, _Page);

    function topGamesPage() {
        _classCallCheck(this, topGamesPage);

        return _possibleConstructorReturn(this, (topGamesPage.__proto__ || Object.getPrototypeOf(topGamesPage)).apply(this, arguments));
    }

    _createClass(topGamesPage, [{
        key: 'squizIosAppsCreateBtn',
        get: function get() {

            return $("[platform*='iOS'] .btn.btn-primary.btn-xl.ng-binding.ng-scope");
        }
    }, {
        key: 'squizAndroidAppsCreateBtn',
        get: function get() {
            return $("[platform*='Android'] .btn.btn-primary.btn-xl.ng-binding.ng-scope .fa-plus-square");
        }
    }, {
        key: 'squizIosAppsToasterBtn',
        get: function get() {
            return $("[platform*='iOS'] .btn.btn-primary.btn-xl.pull-right.ng-scope");
        }
    }, {
        key: 'squizAndroidAppsToasterBtn',
        get: function get() {
            return $("[platform*='Android'] .btn.btn-primary.btn-xl.pull-right.ng-binding.ng-scope");
        }
    }, {
        key: 'topGamesButtonFooter',
        get: function get() {
            return $("#gamesLobbyContainer");
        }
    }, {
        key: 'rouletteButtonFooter',
        get: function get() {
            return $("a.roulette");
        }
    }, {
        key: 'topGamesMainContent',
        get: function get() {
            return $(".topgames__main-content");
        }
    }, {
        key: 'gamesLobbyContainer',
        get: function get() {
            return $("#gamesLobbyContainer");
        }
    }, {
        key: 'appsToasterContainer',
        get: function get() {
            return $("#top-games div.apps-toaster.forIos");
        }
    }, {
        key: 'footerMoreGamesIcon',
        get: function get() {
            return $("a.topGamesButton i.toolbar__icon.icon-MoreGames");
        }
    }, {
        key: 'gamesSash',
        get: function get() {
            return $$(".topgames-game-icon__sash");
        }
    }, {
        key: 'notifications',
        get: function get() {
            return $$(".xsell-notification.xsell-notification--game");
        }
    }]);

    return topGamesPage;
}(_page2.default);

exports.default = new topGamesPage();
//# sourceMappingURL=topGames.page.js.map