'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _chai = require('chai');

var _page = require('./page');

var _page2 = _interopRequireDefault(_page);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SearchPage = function (_Page) {
    _inherits(SearchPage, _Page);

    function SearchPage() {
        _classCallCheck(this, SearchPage);

        return _possibleConstructorReturn(this, (SearchPage.__proto__ || Object.getPrototypeOf(SearchPage)).apply(this, arguments));
    }

    _createClass(SearchPage, [{
        key: 'open',
        value: function open() {
            return _get(SearchPage.prototype.__proto__ || Object.getPrototypeOf(SearchPage.prototype), 'open', this).call(this, 'login');
        }
    }, {
        key: 'submit',
        value: function submit() {
            return this.form.submitForm();
        }
    }, {
        key: 'doSearch',
        value: function doSearch(searchKeyword) {
            this.openSearch();
            this.searchField.waitForVisible(5000);
            return this.searchField.setValue(searchKeyword);
        }
    }, {
        key: 'doSearchOnMobile',
        value: function doSearchOnMobile(searchKeyword) {
            this.openSearchOnMobile();
            this.searchField.waitForVisible(5000);
            return this.searchField.setValue(searchKeyword);
        }
    }, {
        key: 'doAnotherSearch',
        value: function doAnotherSearch(searchKeyword) {
            return this.searchField.setValue(searchKeyword);
        }
    }, {
        key: 'verifyNoSearchEvents',
        value: function verifyNoSearchEvents() {
            return (0, _chai.expect)(this.multipleSearchEventList).to.be.empty;
        }
    }, {
        key: 'verifyNoSearchResults',
        value: function verifyNoSearchResults() {
            return (0, _chai.expect)(this.multipleSearchResults).to.be.empty;
        }
    }, {
        key: 'doNegativeSearch',
        value: function doNegativeSearch(searchKeywords) {
            this.openSearch();
            this.searchField.setValue(searchKeywords);
            this.resultNotFoundPlcHolder.waitForVisible();
            return (0, _chai.expect)(this.resultNotFoundPlcHolder.getText()).to.be.eql('No results found');
        }
    }, {
        key: 'openSearch',
        value: function openSearch() {
            this.waitForOverlayToDisappear();
            this.searchIcon.waitForVisible();
            this.searchIcon.click();
            return this.searchField.waitForVisible(5000);
        }
    }, {
        key: 'openSearchOnMobile',
        value: function openSearchOnMobile() {
            this.mobSearchIcon.waitForVisible();
            this.mobSearchIcon.click();
            return this.searchField.waitForVisible(5000);
        }
    }, {
        key: 'addMaxSearchItems',
        value: function addMaxSearchItems(searchTerm) {
            this.searchField.clearElement();
            this.searchField.setValue(searchTerm);
            browser.pause(2000);
            this.searchEventCount.waitForVisible();
            return this.searchOverlayFirstElement.waitForVisible();
        }
    }, {
        key: 'verifyMaxRecentSearches',
        value: function verifyMaxRecentSearches(noOfSearches) {
            (0, _chai.expect)(this.multipleSearchResults.length).to.be.at.most(noOfSearches);
            return (0, _chai.expect)(this.multipleSearchResults.length).to.be.closeTo(parseInt(noOfSearches), 1);
        }
    }, {
        key: 'verifyMaxRecentSearchesAreNotZero',
        value: function verifyMaxRecentSearchesAreNotZero() {
            return (0, _chai.expect)(this.multipleSearchResults.length).to.be.greaterThan(0);
        }
    }, {
        key: 'useRecentSearch',
        value: function useRecentSearch(searchKeyword) {
            this.searchField.setValue(searchKeyword);
            browser.pause(3000);
            this.searchEventCount.waitForVisible();
            this.searchOverlayFirstElement.waitForVisible();
            this.scroll();
            return this.closeButton.click();
        }

        // Scroll results using jquery

    }, {
        key: 'scroll',
        value: function scroll() {
            return browser.execute('$(".search-events__item").get(1).scrollIntoView();');
        }
    }, {
        key: 'selectFirstOdd',
        value: function selectFirstOdd() {
            this.betOddsButtons.waitForVisible();
            this.betOddsButtons.click();
        }
    }, {
        key: 'searchIcon',
        get: function get() {
            return $('#desktop-sidebar-head.c-list.c-list--icon [class="icon-search"]');
        }
    }, {
        key: 'mobSearchIcon',
        get: function get() {
            return $('.header__icon.icon-search');
        }
    }, {
        key: 'searchField',
        get: function get() {
            return $('#search-input');
        }
    }, {
        key: 'newSearchEventResult',
        get: function get() {
            return $('.search-results-events');
        }
    }, {
        key: 'searchResultSports',
        get: function get() {
            return $('.search-results-sports');
        }
    }, {
        key: 'searchEventResult',
        get: function get() {
            return $('.search-events.search-events--has-results');
        }
    }, {
        key: 'searchEventCount',
        get: function get() {
            return $('.search-events__count');
        }
    }, {
        key: 'searchResultLastItem',
        get: function get() {
            return $('li.search-recent__item:last-of-type .search-recent__name');
        }
    }, {
        key: 'searchResults',
        get: function get() {
            return $('.search-recent__name');
        }
    }, {
        key: 'searchContainer',
        get: function get() {
            return $('.search-events__list');
        }
    }, {
        key: 'searchOverlayFirstElement',
        get: function get() {
            return $('#search-overlay  div div:nth-of-type(2) div:nth-of-type(2) ul li:nth-of-type(1) a');
        }
    }, {
        key: 'recentSearchHeader',
        get: function get() {
            return $('.search-recent__msg');
        }
    }, {
        key: 'resultNotFoundPlcHolder',
        get: function get() {
            return $('.search-overlay__results-not-found');
        }
    }, {
        key: 'topRecentSearch',
        get: function get() {
            return $('.search-recent__item:nth-child(1) .search-recent__name');
        }
    }, {
        key: 'multipleSearchEventList',
        get: function get() {
            return $$('.search-events__list');
        }
    }, {
        key: 'multipleSearchResults',
        get: function get() {
            return $$('.search-recent__name');
        }
    }, {
        key: 'searchEventsResults',
        get: function get() {
            return $$('.search-events__item a');
        }
    }, {
        key: 'searchPreloader',
        get: function get() {
            return $('.search-overlay__loader');
        }
    }, {
        key: 'closeButton',
        get: function get() {
            return $('#search-close');
        }
    }, {
        key: 'betOddsButtons',
        get: function get() {
            return $('.btn.betbutton.oddsbutton.betbutton--alternate');
        }
    }, {
        key: 'addTobetSlipConfirmation',
        get: function get() {
            return $('.add-to-slip-confirmation__added');
        }
    }, {
        key: 'removeFromBetSlipConfirmation',
        get: function get() {
            return $('.add-to-slip-confirmation__removed.add-to-slip-confirmation__removed--animate');
        }
    }]);

    return SearchPage;
}(_page2.default);

exports.default = new SearchPage();
//# sourceMappingURL=search.page.js.map