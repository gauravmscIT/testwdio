'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _chai = require('chai');

var _page = require('./page');

var _page2 = _interopRequireDefault(_page);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var config = require('config');

var bannersPage = function (_Page) {
    _inherits(bannersPage, _Page);

    function bannersPage() {
        _classCallCheck(this, bannersPage);

        return _possibleConstructorReturn(this, (bannersPage.__proto__ || Object.getPrototypeOf(bannersPage)).apply(this, arguments));
    }

    _createClass(bannersPage, [{
        key: 'buildRawBannerService',
        value: function buildRawBannerService(sportsfield) {

            var endPoint = config.get(process.env.npm_config_env + '.bannersEndPoint');
            return endPoint + "/banners/%2Fbetting%2F" + "en-gb" + "%2F" + sportsfield;
        }
    }, {
        key: 'buildBannerFeaturedService',
        value: function buildBannerFeaturedService(sportsfield) {

            var endPoint = config.get(process.env.npm_config_env + '.bannersEndPoint');
            return endPoint + "/banners/%2Fbetting%2F" + "en-gb" + "%2F" + sportsfield + "/featured";
        }
    }, {
        key: 'buildBannerHomepageService',
        value: function buildBannerHomepageService() {

            var endPoint = config.get(process.env.npm_config_env + '.bannersEndPoint');
            return endPoint + "/banners/%2Fbetting%2F" + "en-gb";
        }
    }, {
        key: 'buildBannerTopGamesOverlay',
        value: function buildBannerTopGamesOverlay() {

            var endPoint = config.get(process.env.npm_config_env + '.bannersEndPoint');
            return endPoint + "/banners/%2Fbetting%2F" + "en-gb" + "%2F" + "general/topgames";
        }
    }, {
        key: 'rouletteIcon',
        get: function get() {
            return $('#xsell-roulette');
        }
    }, {
        key: 'bnrsCarouselLnkContainers',
        get: function get() {
            return $$('.segmentation.forWeb .banners-carousel__link');
        }
    }, {
        key: 'sqzTopGamesOverlayBtn',
        get: function get() {
            return $('.md-tab.ng-scope.ng-isolate-scope.md-ink-ripple.md-active');
        }
    }, {
        key: 'bannerLinks',
        get: function get() {
            return $$('.topgames__banner .banners-container .segmentation.forWeb');
        }
    }, {
        key: 'bannersCarousels',
        get: function get() {
            return $$('.cpt-banner-enhanced.cpt-banner-enhanced--link [data-trk-carousel-index]');
        }
    }, {
        key: 'bannersForWeb',
        get: function get() {
            return $$('.banners-container .segmentation.forWeb .cpt-banner-enhanced.cpt-banner-enhanced--gameLauncher .non-route');
        }
    }, {
        key: 'bannersForNative',
        get: function get() {
            return $$('.banners-container .segmentation.forNative .cpt-banner-enhanced.cpt-banner-enhanced--gameLauncher .non-route');
        }
    }, {
        key: 'dataTrkBanners',
        get: function get() {
            return $$('article.cpt-banner-enhanced--link div button');
        }
    }, {
        key: 'dataTrkBannersArticle',
        get: function get() {
            return $$('article.cpt-banner-enhanced--link div a:nth-of-type(1)');
        }
    }]);

    return bannersPage;
}(_page2.default);

exports.default = new bannersPage();
//# sourceMappingURL=banners.page.js.map