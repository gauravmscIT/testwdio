'use strict';

exports.config = {

    specs: ['./features/**/*.feature'],
    // path to BDD Feature files

    maxInstances: 10,
    //max instances


    capabilities: [{
        maxInstances: 5,
        //
        browserName: 'chrome',
        chromeOptions: {
            args: ['--window-size=1280,1024']
        }
    }],
    sync: true,
    // Level of logging verbosity: silent | verbose | command | data | result | error
    logLevel: 'silent',
    // Enables colours for log output.
    coloredLogs: true,
    //
    // If you only want to run your tests until a specific amount of tests have failed use
    // bail (default is 0 - don't bail, run all tests).
    bail: 0,
    //
    // Saves a screenshot to a given path if a command fails.
    screenshotPath: './errorShots/',
    //
    // Set a base URL in order to shorten url command calls. If your url parameter starts
    // with "/", then the base url gets prepended.
    baseUrl: 'https://www.yld.io',
    //
    // Default timeout for all waitFor* commands.
    waitforTimeout: 10000,
    //
    // Default timeout in milliseconds for request
    // if Selenium Grid doesn't send response
    // connectionRetryTimeout: 90000,
    //
    services: ['selenium-standalone'],

    framework: 'cucumber',
    //
    // Test reporter for stdout.
    // The only one supported by default is 'dot'
    // see also: http://webdriver.io/guide/testrunner/reporters.html

    reporters: ['allure', 'cucumber'],
    reporterOptions: {
        allure: {
            outputDir: 'allure-results'
        }
    },

    cucumberOpts: {

        require: ['./features/step-definitions'],
        backtrace: false, // <boolean> show full backtrace for errors
        compiler: [], // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
        dryRun: false, // <boolean> invoke formatters without executing steps
        failFast: false, // <boolean> abort the run on first failure
        format: ['pretty'], // <string[]> (type[:path]) specify the output format, optionally supply PATH to redirect formatter output (repeatable)
        colors: true, // <boolean> disable colors in formatter output
        snippets: true, // <boolean> hide step definition snippets for pending steps
        source: true, // <boolean> hide source uris
        profile: [], // <string[]> (name) specify the profile to use
        strict: false, // <boolean> fail if there are any undefined or pending steps
        timeout: 35000, // <number> timeout for step definitions
        ignoreUndefinedDefinitions: false // <boolean> Enable this config to treat undefined definitions as warnings.
    },

    beforeSession: function beforeSession() {
        require('babel-register');
        require('isomorphic-fetch');
        require('es6-promise');
        //Before Hook
    }
};
//# sourceMappingURL=wdio.wdioConf.js.map