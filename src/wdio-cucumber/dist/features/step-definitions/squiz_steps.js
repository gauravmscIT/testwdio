'use strict';

var _chai = require('chai');

var _squiz = require('../../pageobjects/squiz.page');

var _squiz2 = _interopRequireDefault(_squiz);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var config = require('config');
var fieldValue = void 0;

var JSONPath = require('advanced-json-path');
var endPoint = void 0;
var iconTitles = [];
var squizUserName = 'Gaurav.Kumar';
var successMsg = "Notification was created successfully.";
var editSuccessMsg = "Notification was updated successfully.";

var notificationName = void 0;
var ceiUrl = void 0;

module.exports = function () {
    var _this = this;

    this.When(/^I navigate to CEI for carousel navigation items$/, function () {
        var ceiUrl = _squiz2.default.goToCeiUrl('.squiz_url');
        return _squiz2.default.loginToSquiz(squizUserName, process.env.npm_config_pass);
    });

    this.When(/^I click to create a new carousel navigation item$/, _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
        return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        ceiUrl = _squiz2.default.goToCeiUrl('.squizCarouselNav_url');
                        _squiz2.default.squizAddNewItemBtn.waitForVisible();
                        _squiz2.default.squizAddNewItemBtn.click();

                    case 3:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, _this);
    })));

    this.When(/^I click to create a new notification item$/, _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        ceiUrl = _squiz2.default.goToCeiUrl('.squizNotification_url');
                        _squiz2.default.squizAddNewNotificationBtn.waitForVisible();
                        _squiz2.default.squizAddNewNotificationBtn.click();

                    case 3:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, _this);
    })));

    this.Then(/^the following fields are displayed$/, function (table) {
        return table.raw().forEach(function (item) {
            _squiz2.default.verifyText(item);
        });
    });

    this.When(/^I should log out$/, function () {
        _squiz2.default.scrollToLogout();
        _squiz2.default.squizLogoutButton.waitForVisible();
        _squiz2.default.squizLogoutButton.click();
    });

    this.When(/^I obtain list of icon carousels from the endpoint$/, _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
        var response, data, titles;
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
            while (1) {
                switch (_context3.prev = _context3.next) {
                    case 0:
                        _this.endPoint = config.get(process.env.npm_config_env + '.carouselEndPoint');
                        _context3.next = 3;
                        return fetch(_this.endPoint);

                    case 3:
                        response = _context3.sent;

                        (0, _chai.expect)(response.status).to.be.eql(200);
                        _context3.next = 7;
                        return response.json();

                    case 7:
                        data = _context3.sent;
                        titles = "$.data[?(@.enabled==true)].title";

                        iconTitles = JSONPath(data, titles);
                        (0, _chai.expect)(iconTitles).to.have.length.above(0);

                    case 11:
                    case 'end':
                        return _context3.stop();
                }
            }
        }, _callee3, _this);
    })));

    this.Then(/^the icon carousels should be present and match the endpoint$/, function () {
        var frontendIconTitles = [];
        _squiz2.default.iconCarouselTitles[0].waitForVisible();
        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
            for (var _iterator = _squiz2.default.iconCarouselTitles[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                var obj = _step.value;

                if (obj.isVisible()) {
                    frontendIconTitles.push(obj.getText());
                }
            }
        } catch (err) {
            _didIteratorError = true;
            _iteratorError = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion && _iterator.return) {
                    _iterator.return();
                }
            } finally {
                if (_didIteratorError) {
                    throw _iteratorError;
                }
            }
        }

        for (var i = 0; i < frontendIconTitles.length; i++) {
            (0, _chai.expect)(frontendIconTitles[i]).to.be.eql(iconTitles[i]);
        }
        (0, _chai.expect)(frontendIconTitles.length).to.be.eql(iconTitles.length);
    });

    this.When(/^I create a new "([^"]*)" notification$/, function (keyword) {
        notificationName = "Test Notification " + Math.random().toString(36);
        return _squiz2.default.createNotification(keyword, notificationName);
    });

    this.When(/^I create a new Game notification by linking a game$/, _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
        return regeneratorRuntime.wrap(function _callee4$(_context4) {
            while (1) {
                switch (_context4.prev = _context4.next) {
                    case 0:
                        return _context4.abrupt('return', _squiz2.default.createGameNtfnWithLink());

                    case 1:
                    case 'end':
                        return _context4.stop();
                }
            }
        }, _callee4, _this);
    })));

    this.Then(/^the notification should be created$/, _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
        return regeneratorRuntime.wrap(function _callee5$(_context5) {
            while (1) {
                switch (_context5.prev = _context5.next) {
                    case 0:
                        _squiz2.default.notificationActionMessage.waitForVisible();
                        return _context5.abrupt('return', (0, _chai.expect)(_squiz2.default.notificationActionMessage.getText()).to.include(successMsg));

                    case 2:
                    case 'end':
                        return _context5.stop();
                }
            }
        }, _callee5, _this);
    })));

    this.Then(/^the notification should be updated$/, _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
        return regeneratorRuntime.wrap(function _callee6$(_context6) {
            while (1) {
                switch (_context6.prev = _context6.next) {
                    case 0:
                        _squiz2.default.notificationActionMessage.waitForVisible();
                        return _context6.abrupt('return', (0, _chai.expect)(_squiz2.default.notificationActionMessage.getText()).to.include(editSuccessMsg));

                    case 2:
                    case 'end':
                        return _context6.stop();
                }
            }
        }, _callee6, _this);
    })));

    this.When(/^I edit the newly created notification and save$/, _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
        return regeneratorRuntime.wrap(function _callee7$(_context7) {
            while (1) {
                switch (_context7.prev = _context7.next) {
                    case 0:
                        browser.url(ceiUrl);
                        _squiz2.default.squizAddNewNotificationBtn.waitForVisible();
                        _squiz2.default.notificationNames.forEach(function (value, i) {
                            if (value.getText() == notificationName) {
                                _squiz2.default.notificationEditButtons[i].click();
                            }
                        });
                        _squiz2.default.editNotification();

                    case 4:
                    case 'end':
                        return _context7.stop();
                }
            }
        }, _callee7, _this);
    })));

    this.When(/^I should be able to delete the newly created notification$/, _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
        return regeneratorRuntime.wrap(function _callee8$(_context8) {
            while (1) {
                switch (_context8.prev = _context8.next) {
                    case 0:
                        browser.url(ceiUrl);
                        _squiz2.default.squizAddNewNotificationBtn.waitForVisible();
                        _squiz2.default.scrollToCreateNotification();
                        _squiz2.default.notificationNames.forEach(function (value, i) {
                            if (value.getText() == notificationName) {
                                _squiz2.default.notificationDeleteButtons[i].click();
                                if (browser.alertText()) {
                                    browser.alertAccept();
                                }
                            }
                        });

                    case 4:
                    case 'end':
                        return _context8.stop();
                }
            }
        }, _callee8, _this);
    })));

    this.When(/^I should be able to delete the newly edited notification$/, _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee9() {
        return regeneratorRuntime.wrap(function _callee9$(_context9) {
            while (1) {
                switch (_context9.prev = _context9.next) {
                    case 0:
                        browser.url(ceiUrl);
                        _squiz2.default.squizAddNewNotificationBtn.waitForVisible();
                        _squiz2.default.notificationNames.forEach(function (value, i) {
                            if (value.getText() == notificationName) {
                                _squiz2.default.notificationDeleteButtons[i].click();
                                if (browser.alertText()) {
                                    browser.alertAccept();
                                }
                            }
                        });

                    case 3:
                    case 'end':
                        return _context9.stop();
                }
            }
        }, _callee9, _this);
    })));
};
//# sourceMappingURL=squiz_steps.js.map