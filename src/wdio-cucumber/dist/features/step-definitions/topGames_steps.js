'use strict';

var _chai = require('chai');

var _topGames = require('../../pageobjects/topGames.page');

var _topGames2 = _interopRequireDefault(_topGames);

var _banners = require('../../pageobjects/banners.page');

var _banners2 = _interopRequireDefault(_banners);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var config = require('config');

var JSONPath = require('advanced-json-path');

var endPoint = void 0;
var activeStatus = [];
var response = void 0;
var gameIdsWithSash = [];
var noOfSashesInEndPoint = void 0;
var activeNotisIDs = [];

var bannerTitles = [];
var mobileUrls = [];
var imageUrls = [];
var imgLargeUrls = [];
var imgSmallUrls = [];

var desktopUrls = [];
var nativeMobileUrls = [];
var nativeDesktopUrls = [];
var ceiMobileUrls = [];
var ceiDesktopUrls = [];

var ceiImageUrls = [];
var ceiImgLargeUrls = [];
var ceiImgSmallUrls = [];

module.exports = function () {
    var _this = this;

    this.Then(/^the options to create apps and apps toasters should be present$/, function () {

        _topGames2.default.squizIosAppsCreateBtn.waitForVisible();
        (0, _chai.expect)(_topGames2.default.squizIosAppsCreateBtn.isVisible()).to.be.eql(true);
        // expect(topGamesPage.squizAndroidAppsCreateBtn.isVisible()).to.be.eql(true);
        (0, _chai.expect)(_topGames2.default.squizIosAppsToasterBtn.isVisible()).to.be.eql(true);
        // expect(topGamesPage.squizAndroidAppsToasterBtn.isVisible()).to.be.eql(true);

    });

    this.Then(/^I should be able to use more games button in the footer$/, function () {
        _topGames2.default.topGamesButtonFooter.waitForVisible();
        _topGames2.default.topGamesButtonFooter.click();
        _topGames2.default.topGamesMainContent.waitForVisible();
        (0, _chai.expect)(_topGames2.default.topGamesMainContent.isVisible()).to.be.eql(true);
    });

    this.Then(/^the Top Games button in the footer should be hidden$/, function () {
        (0, _chai.expect)(_topGames2.default.topGamesButtonFooter.getAttribute('class')).to.include("wh-hide");
        (0, _chai.expect)(_topGames2.default.topGamesButtonFooter.isVisible()).to.be.eql(false);
    });

    this.Then(/^the more games icon should be present in the footer$/, function () {
        (0, _chai.expect)(_topGames2.default.footerMoreGamesIcon.isVisible()).to.be.eql(true);
    });

    this.Given(/^I get the values from games lobby fragment$/, _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
        var response, data, toasterJson;
        return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _this.endPoint = config.get(process.env.npm_config_env + '.gamesLobbyFragment');
                        _context.next = 3;
                        return fetch(_this.endPoint);

                    case 3:
                        response = _context.sent;

                        (0, _chai.expect)(response.status).to.be.eql(200);
                        _context.next = 7;
                        return response.json();

                    case 7:
                        data = _context.sent;
                        toasterJson = "$.data[?(@.enabled==true)].appsByPlatform.ios.toasterActive";

                        activeStatus = JSONPath(data, toasterJson);

                    case 10:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, _this);
    })));

    this.When(/^I open the top games overlay$/, function () {
        _topGames2.default.topGamesButtonFooter.waitForVisible();
        _topGames2.default.topGamesButtonFooter.click();
        _topGames2.default.topGamesMainContent.waitForVisible();
        (0, _chai.expect)(_topGames2.default.topGamesMainContent.isVisible()).to.be.eql(true);
    });

    this.Then(/^the apps and apps toaster should appear as per fragment values$/, function () {
        if (activeStatus) {
            _topGames2.default.appsToasterContainer.waitForVisible();
            (0, _chai.expect)(_topGames2.default.appsToasterContainer.isVisible()).to.be.eql(true);
        } else {
            (0, _chai.expect)(_topGames2.default.appsToasterContainer.isVisible()).to.be.eql(false);
        }
    });

    this.Given(/^I hit the squiz endpoint$/, _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        _this.endPoint = config.get(process.env.npm_config_env + '.squizTopGamesEndpoint');
                        _context2.next = 3;
                        return fetch(_this.endPoint);

                    case 3:
                        response = _context2.sent;

                        (0, _chai.expect)(response.status).to.be.eql(200);

                    case 5:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, _this);
    })));

    this.Given(/^I obtain the games IDs with active stash$/, _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
        var currentTimeStamp, data, ids, sectionIds, gameIds, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, sectionId, stashActiveGames;

        return regeneratorRuntime.wrap(function _callee3$(_context3) {
            while (1) {
                switch (_context3.prev = _context3.next) {
                    case 0:
                        currentTimeStamp = Math.floor(Date.now());
                        _context3.next = 3;
                        return response.json();

                    case 3:
                        data = _context3.sent;
                        ids = "$.sections[?(@.active==true)].id";
                        sectionIds = JSONPath(data, ids);
                        gameIds = void 0;
                        _iteratorNormalCompletion = true;
                        _didIteratorError = false;
                        _iteratorError = undefined;
                        _context3.prev = 10;

                        for (_iterator = sectionIds[Symbol.iterator](); !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                            sectionId = _step.value;
                            stashActiveGames = "$.sections[?(@.id==" + sectionId + ")].games[?(@.sashActive==true)][?(@.active==true)][?(@.sashToDateTime>=" + currentTimeStamp + ")][?(@.sashFromDateTime<=" + currentTimeStamp + ")].id";

                            gameIds = JSONPath(data, stashActiveGames);
                            if (!gameIds === false) {
                                gameIdsWithSash = gameIdsWithSash.concat(Array.isArray(gameIds) ? gameIds : [gameIds]);
                            }
                        }
                        _context3.next = 18;
                        break;

                    case 14:
                        _context3.prev = 14;
                        _context3.t0 = _context3['catch'](10);
                        _didIteratorError = true;
                        _iteratorError = _context3.t0;

                    case 18:
                        _context3.prev = 18;
                        _context3.prev = 19;

                        if (!_iteratorNormalCompletion && _iterator.return) {
                            _iterator.return();
                        }

                    case 21:
                        _context3.prev = 21;

                        if (!_didIteratorError) {
                            _context3.next = 24;
                            break;
                        }

                        throw _iteratorError;

                    case 24:
                        return _context3.finish(21);

                    case 25:
                        return _context3.finish(18);

                    case 26:
                        noOfSashesInEndPoint = gameIdsWithSash.length;

                    case 27:
                    case 'end':
                        return _context3.stop();
                }
            }
        }, _callee3, _this, [[10, 14, 18, 26], [19,, 21, 25]]);
    })));

    this.Then(/^the sashes should match the squiz endpoint$/, _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
        return regeneratorRuntime.wrap(function _callee4$(_context4) {
            while (1) {
                switch (_context4.prev = _context4.next) {
                    case 0:
                        if (noOfSashesInEndPoint === 0) {
                            (0, _chai.expect)(_topGames2.default.gamesSash.length).to.be.eql(noOfSashesInEndPoint);
                        } else {
                            _topGames2.default.gamesSash[0].waitForVisible();
                            (0, _chai.expect)(_topGames2.default.gamesSash.length).to.be.eql(noOfSashesInEndPoint);
                        }

                    case 1:
                    case 'end':
                        return _context4.stop();
                }
            }
        }, _callee4, _this);
    })));

    this.When(/^there is active notification content in api$/, _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
        var obtainedNotis, currentTimeStamp, data, notifications;
        return regeneratorRuntime.wrap(function _callee5$(_context5) {
            while (1) {
                switch (_context5.prev = _context5.next) {
                    case 0:
                        obtainedNotis = [];
                        currentTimeStamp = Math.floor(Date.now());

                        _this.endPoint = config.get(process.env.npm_config_env + '.squizNotificationJsonEndpoint');
                        _context5.next = 5;
                        return fetch(_this.endPoint);

                    case 5:
                        response = _context5.sent;

                        (0, _chai.expect)(response.status).to.be.eql(200);
                        _context5.next = 9;
                        return response.json();

                    case 9:
                        data = _context5.sent;
                        notifications = "$.notifications[?(@.active==true)][?(@.toDateTime>=" + currentTimeStamp + ")][?(@.fromDateTime<=" + currentTimeStamp + ")].id";

                        obtainedNotis = JSONPath(data, notifications);
                        if (!obtainedNotis === false) {
                            activeNotisIDs = activeNotisIDs.concat(Array.isArray(obtainedNotis) ? obtainedNotis : [obtainedNotis]);
                        }
                        if (activeNotisIDs.length === 0) {
                            activeNotisIDs = [];
                        }

                    case 14:
                    case 'end':
                        return _context5.stop();
                }
            }
        }, _callee5, _this);
    })));

    this.Then(/^I should see the same notifications$/, function () {
        var feNotifs = [];
        var res = void 0;
        if (activeNotisIDs.length > 0) {
            _topGames2.default.notifications[0].waitForVisible();
            feNotifs = _topGames2.default.notifications.map(function (obj) {
                return obj.getAttribute('data-id');
            });
        }
        res = activeNotisIDs.length == feNotifs.length && activeNotisIDs.every(function (v, i) {
            return v == feNotifs[i];
        });
        (0, _chai.expect)(res).to.be.eql(true);
    });

    this.When(/^I obtain game launcher values from banner service call$/, function () {
        _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
            var response, data, typeValue, mobileUrlPath, desktopUrlPath;
            return regeneratorRuntime.wrap(function _callee6$(_context6) {
                while (1) {
                    switch (_context6.prev = _context6.next) {
                        case 0:

                            _this.endPoint = _banners2.default.buildBannerTopGamesOverlay();
                            _context6.next = 3;
                            return fetch(_this.endPoint);

                        case 3:
                            response = _context6.sent;

                            (0, _chai.expect)(response.status).to.be.eql(200);
                            _context6.next = 7;
                            return response.json();

                        case 7:
                            data = _context6.sent;
                            typeValue = "gameLauncher";
                            mobileUrlPath = "$.data[?(@.type==\"" + typeValue + "\")].content.gameLauncher.url.mobile";
                            desktopUrlPath = "$.data[?(@.type==\"" + typeValue + "\")].content.gameLauncher.url.desktop";

                            ceiMobileUrls = JSONPath(data, mobileUrlPath);
                            ceiDesktopUrls = JSONPath(data, desktopUrlPath);
                            console.log("Data " + ceiDesktopUrls);
                            return _context6.abrupt('return', [ceiMobileUrls, ceiDesktopUrls]);

                        case 15:
                        case 'end':
                            return _context6.stop();
                    }
                }
            }, _callee6, _this);
        }));
    });

    this.Then(/^the banners attributes for game launcher should match the banner service$/, function () {
        _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
            var _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, banner, _iteratorNormalCompletion3, _didIteratorError3, _iteratorError3, _iterator3, _step3, _banner, _iteratorNormalCompletion4, _didIteratorError4, _iteratorError4, _iterator4, _step4, mobileUrl, _iteratorNormalCompletion5, _didIteratorError5, _iteratorError5, _iterator5, _step5, _mobileUrl, _iteratorNormalCompletion6, _didIteratorError6, _iteratorError6, _iterator6, _step6, desktopUrl, _iteratorNormalCompletion7, _didIteratorError7, _iteratorError7, _iterator7, _step7, _mobileUrl2, _iteratorNormalCompletion8, _didIteratorError8, _iteratorError8, _iterator8, _step8, _desktopUrl;

            return regeneratorRuntime.wrap(function _callee7$(_context7) {
                while (1) {
                    switch (_context7.prev = _context7.next) {
                        case 0:
                            browser.waitForVisible('.topgames__banner .banners-container .segmentation.forWeb', 8000);

                            _iteratorNormalCompletion2 = true;
                            _didIteratorError2 = false;
                            _iteratorError2 = undefined;
                            _context7.prev = 4;
                            for (_iterator2 = _banners2.default.bannersForWeb[Symbol.iterator](); !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                                banner = _step2.value;

                                console.log("More Data" + banner.getAttribute('mobile-url'));
                                mobileUrls.push(banner.getAttribute('mobile-url'));
                                desktopUrls.push(banner.getAttribute('desktop-url'));
                            }
                            _context7.next = 12;
                            break;

                        case 8:
                            _context7.prev = 8;
                            _context7.t0 = _context7['catch'](4);
                            _didIteratorError2 = true;
                            _iteratorError2 = _context7.t0;

                        case 12:
                            _context7.prev = 12;
                            _context7.prev = 13;

                            if (!_iteratorNormalCompletion2 && _iterator2.return) {
                                _iterator2.return();
                            }

                        case 15:
                            _context7.prev = 15;

                            if (!_didIteratorError2) {
                                _context7.next = 18;
                                break;
                            }

                            throw _iteratorError2;

                        case 18:
                            return _context7.finish(15);

                        case 19:
                            return _context7.finish(12);

                        case 20:
                            mobileUrls = mobileUrls.filter(function (item, index, inputArray) {
                                return inputArray.indexOf(item) === index;
                            });
                            desktopUrls = desktopUrls.filter(function (item, index, inputArray) {
                                return inputArray.indexOf(item) === index;
                            });

                            _iteratorNormalCompletion3 = true;
                            _didIteratorError3 = false;
                            _iteratorError3 = undefined;
                            _context7.prev = 25;
                            for (_iterator3 = _banners2.default.bannersForNative[Symbol.iterator](); !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                                _banner = _step3.value;

                                nativeMobileUrls.push(_banner.getAttribute('mobile-url'));
                                nativeDesktopUrls.push(_banner.getAttribute('desktop-url'));
                            }
                            _context7.next = 33;
                            break;

                        case 29:
                            _context7.prev = 29;
                            _context7.t1 = _context7['catch'](25);
                            _didIteratorError3 = true;
                            _iteratorError3 = _context7.t1;

                        case 33:
                            _context7.prev = 33;
                            _context7.prev = 34;

                            if (!_iteratorNormalCompletion3 && _iterator3.return) {
                                _iterator3.return();
                            }

                        case 36:
                            _context7.prev = 36;

                            if (!_didIteratorError3) {
                                _context7.next = 39;
                                break;
                            }

                            throw _iteratorError3;

                        case 39:
                            return _context7.finish(36);

                        case 40:
                            return _context7.finish(33);

                        case 41:
                            nativeMobileUrls = nativeMobileUrls.filter(function (item, index, inputArray) {
                                return inputArray.indexOf(item) === index;
                            });
                            nativeDesktopUrls = nativeDesktopUrls.filter(function (item, index, inputArray) {
                                return inputArray.indexOf(item) === index;
                            });

                            _iteratorNormalCompletion4 = true;
                            _didIteratorError4 = false;
                            _iteratorError4 = undefined;
                            _context7.prev = 46;
                            for (_iterator4 = mobileUrls[Symbol.iterator](); !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                                mobileUrl = _step4.value;


                                (0, _chai.expect)(ceiMobileUrls).to.include(mobileUrl);
                            }
                            _context7.next = 54;
                            break;

                        case 50:
                            _context7.prev = 50;
                            _context7.t2 = _context7['catch'](46);
                            _didIteratorError4 = true;
                            _iteratorError4 = _context7.t2;

                        case 54:
                            _context7.prev = 54;
                            _context7.prev = 55;

                            if (!_iteratorNormalCompletion4 && _iterator4.return) {
                                _iterator4.return();
                            }

                        case 57:
                            _context7.prev = 57;

                            if (!_didIteratorError4) {
                                _context7.next = 60;
                                break;
                            }

                            throw _iteratorError4;

                        case 60:
                            return _context7.finish(57);

                        case 61:
                            return _context7.finish(54);

                        case 62:
                            _iteratorNormalCompletion5 = true;
                            _didIteratorError5 = false;
                            _iteratorError5 = undefined;
                            _context7.prev = 65;
                            for (_iterator5 = mobileUrls[Symbol.iterator](); !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
                                _mobileUrl = _step5.value;


                                (0, _chai.expect)(ceiMobileUrls).to.include(_mobileUrl);
                            }
                            _context7.next = 73;
                            break;

                        case 69:
                            _context7.prev = 69;
                            _context7.t3 = _context7['catch'](65);
                            _didIteratorError5 = true;
                            _iteratorError5 = _context7.t3;

                        case 73:
                            _context7.prev = 73;
                            _context7.prev = 74;

                            if (!_iteratorNormalCompletion5 && _iterator5.return) {
                                _iterator5.return();
                            }

                        case 76:
                            _context7.prev = 76;

                            if (!_didIteratorError5) {
                                _context7.next = 79;
                                break;
                            }

                            throw _iteratorError5;

                        case 79:
                            return _context7.finish(76);

                        case 80:
                            return _context7.finish(73);

                        case 81:
                            _iteratorNormalCompletion6 = true;
                            _didIteratorError6 = false;
                            _iteratorError6 = undefined;
                            _context7.prev = 84;
                            for (_iterator6 = desktopUrls[Symbol.iterator](); !(_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done); _iteratorNormalCompletion6 = true) {
                                desktopUrl = _step6.value;


                                (0, _chai.expect)(ceiDesktopUrls).to.include(desktopUrl);
                            }

                            _context7.next = 92;
                            break;

                        case 88:
                            _context7.prev = 88;
                            _context7.t4 = _context7['catch'](84);
                            _didIteratorError6 = true;
                            _iteratorError6 = _context7.t4;

                        case 92:
                            _context7.prev = 92;
                            _context7.prev = 93;

                            if (!_iteratorNormalCompletion6 && _iterator6.return) {
                                _iterator6.return();
                            }

                        case 95:
                            _context7.prev = 95;

                            if (!_didIteratorError6) {
                                _context7.next = 98;
                                break;
                            }

                            throw _iteratorError6;

                        case 98:
                            return _context7.finish(95);

                        case 99:
                            return _context7.finish(92);

                        case 100:
                            _iteratorNormalCompletion7 = true;
                            _didIteratorError7 = false;
                            _iteratorError7 = undefined;
                            _context7.prev = 103;
                            for (_iterator7 = nativeMobileUrls[Symbol.iterator](); !(_iteratorNormalCompletion7 = (_step7 = _iterator7.next()).done); _iteratorNormalCompletion7 = true) {
                                _mobileUrl2 = _step7.value;


                                (0, _chai.expect)(ceiMobileUrls).to.include(_mobileUrl2);
                            }
                            _context7.next = 111;
                            break;

                        case 107:
                            _context7.prev = 107;
                            _context7.t5 = _context7['catch'](103);
                            _didIteratorError7 = true;
                            _iteratorError7 = _context7.t5;

                        case 111:
                            _context7.prev = 111;
                            _context7.prev = 112;

                            if (!_iteratorNormalCompletion7 && _iterator7.return) {
                                _iterator7.return();
                            }

                        case 114:
                            _context7.prev = 114;

                            if (!_didIteratorError7) {
                                _context7.next = 117;
                                break;
                            }

                            throw _iteratorError7;

                        case 117:
                            return _context7.finish(114);

                        case 118:
                            return _context7.finish(111);

                        case 119:
                            _iteratorNormalCompletion8 = true;
                            _didIteratorError8 = false;
                            _iteratorError8 = undefined;
                            _context7.prev = 122;
                            _iterator8 = nativeDesktopUrls[Symbol.iterator]();

                        case 124:
                            if (_iteratorNormalCompletion8 = (_step8 = _iterator8.next()).done) {
                                _context7.next = 130;
                                break;
                            }

                            _desktopUrl = _step8.value;
                            return _context7.abrupt('return', (0, _chai.expect)(ceiDesktopUrls).to.include(_desktopUrl));

                        case 127:
                            _iteratorNormalCompletion8 = true;
                            _context7.next = 124;
                            break;

                        case 130:
                            _context7.next = 136;
                            break;

                        case 132:
                            _context7.prev = 132;
                            _context7.t6 = _context7['catch'](122);
                            _didIteratorError8 = true;
                            _iteratorError8 = _context7.t6;

                        case 136:
                            _context7.prev = 136;
                            _context7.prev = 137;

                            if (!_iteratorNormalCompletion8 && _iterator8.return) {
                                _iterator8.return();
                            }

                        case 139:
                            _context7.prev = 139;

                            if (!_didIteratorError8) {
                                _context7.next = 142;
                                break;
                            }

                            throw _iteratorError8;

                        case 142:
                            return _context7.finish(139);

                        case 143:
                            return _context7.finish(136);

                        case 144:
                        case 'end':
                            return _context7.stop();
                    }
                }
            }, _callee7, _this, [[4, 8, 12, 20], [13,, 15, 19], [25, 29, 33, 41], [34,, 36, 40], [46, 50, 54, 62], [55,, 57, 61], [65, 69, 73, 81], [74,, 76, 80], [84, 88, 92, 100], [93,, 95, 99], [103, 107, 111, 119], [112,, 114, 118], [122, 132, 136, 144], [137,, 139, 143]]);
        }));
    });
};
//# sourceMappingURL=topGames_steps.js.map