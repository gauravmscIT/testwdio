'use strict';

var _chai = require('chai');

var _banners = require('../../pageobjects/banners.page');

var _banners2 = _interopRequireDefault(_banners);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var dataTrkCTA = 'data-trk-cta';
var dataTrkPosition = "data-trk-position"; //Position banner is located in
var dataTrkGameType = "data-trk-gametype"; //Page name
var dataTrkCarousel = "data-trk-carousel-index";
var dataTrkPlatform = "data-trk-platform";
var dataTrkBannerId = "data-trk-banner-id"; //Alt text


module.exports = function () {

    this.Then(/^the banner is displayed with data-tracking attributes$/, function () {

        if (_banners2.default.dataTrkBanners.length === 0) {
            _banners2.default.dataTrkBannersArticle[0].waitForVisible();
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = _banners2.default.dataTrkBannersArticle[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var obj = _step.value;

                    (0, _chai.expect)(obj.getAttribute(dataTrkCTA) !== null).to.be.eql(true);
                    (0, _chai.expect)(obj.getAttribute(dataTrkPosition) !== null).to.be.eql(true);
                    (0, _chai.expect)(obj.getAttribute(dataTrkGameType) !== null).to.be.eql(true);
                    (0, _chai.expect)(obj.getAttribute(dataTrkCarousel) !== null).to.be.eql(true);
                    (0, _chai.expect)(obj.getAttribute(dataTrkPlatform) !== null).to.be.eql(true);
                    (0, _chai.expect)(obj.getAttribute(dataTrkBannerId) !== null).to.be.eql(true);
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }
        } else {
            var _iteratorNormalCompletion2 = true;
            var _didIteratorError2 = false;
            var _iteratorError2 = undefined;

            try {
                for (var _iterator2 = _banners2.default.dataTrkBanners[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                    var _obj = _step2.value;


                    (0, _chai.expect)(_obj.getAttribute(dataTrkCTA) !== null).to.be.eql(true);
                    (0, _chai.expect)(_obj.getAttribute(dataTrkPosition) !== null).to.be.eql(true);
                    (0, _chai.expect)(_obj.getAttribute(dataTrkGameType) !== null).to.be.eql(true);
                    (0, _chai.expect)(_obj.getAttribute(dataTrkCarousel) !== null).to.be.eql(true);
                    (0, _chai.expect)(_obj.getAttribute(dataTrkPlatform) !== null).to.be.eql(true);
                    (0, _chai.expect)(_obj.getAttribute(dataTrkBannerId) !== null).to.be.eql(true);
                }
            } catch (err) {
                _didIteratorError2 = true;
                _iteratorError2 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion2 && _iterator2.return) {
                        _iterator2.return();
                    }
                } finally {
                    if (_didIteratorError2) {
                        throw _iteratorError2;
                    }
                }
            }
        }
    });
};
//# sourceMappingURL=dataTracking_steps.js.map