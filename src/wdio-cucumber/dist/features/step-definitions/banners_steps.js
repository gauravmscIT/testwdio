'use strict';

var _chai = require('chai');

var _advancedJsonPath = require('advanced-json-path');

var _advancedJsonPath2 = _interopRequireDefault(_advancedJsonPath);

var _config = require('config');

var _config2 = _interopRequireDefault(_config);

var _banners = require('../../pageobjects/banners.page');

var _banners2 = _interopRequireDefault(_banners);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var endPoint = void 0;
var bannerTitles = [];
var mobileUrls = [];
var imageUrls = [];
var imgLargeUrls = [];
var imgSmallUrls = [];

var desktopUrls = [];
var nativeMobileUrls = [];
var nativeDesktopUrls = [];
var ceiMobileUrls = [];
var ceiDesktopUrls = [];

var ceiImageUrls = [];
var ceiImgLargeUrls = [];
var ceiImgSmallUrls = [];

module.exports = function () {
    var _this = this;

    this.Then(/^I call raw-banner service with "([^"]*)"$/, function (field) {
        _this.endPoint = _banners2.default.buildRawBannerService(field);
    });

    this.Then(/^I call featured service with "([^"]*)"$/, function (field) {
        _this.endPoint = _banners2.default.buildBannerFeaturedService(field);
    });

    this.Then(/^the service should return the successful response status code$/, function () {
        _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var response;
            return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                    switch (_context.prev = _context.next) {
                        case 0:
                            _context.next = 2;
                            return fetch(_this.endPoint);

                        case 2:
                            response = _context.sent;

                            (0, _chai.expect)(response.status).to.be.eql(200);

                        case 4:
                        case 'end':
                            return _context.stop();
                    }
                }
            }, _callee, _this);
        }));
    });

    this.Given(/^I call the banner service for homepage$/, function () {
        _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                    switch (_context2.prev = _context2.next) {
                        case 0:
                            _this.endPoint = _banners2.default.buildBannerHomepageService();

                        case 1:
                        case 'end':
                            return _context2.stop();
                    }
                }
            }, _callee2, _this);
        }));
    });

    this.Given(/^I obtain titles of homepage banners$/, function () {

        _banners2.default.bnrsCarouselLnkContainers[0].waitForVisible();

        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
            for (var _iterator = _banners2.default.bnrsCarouselLnkContainers[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                var container = _step.value;

                bannerTitles.push(container.getText());
            }
        } catch (err) {
            _didIteratorError = true;
            _iteratorError = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion && _iterator.return) {
                    _iterator.return();
                }
            } finally {
                if (_didIteratorError) {
                    throw _iteratorError;
                }
            }
        }

        (0, _chai.expect)(bannerTitles).to.not.be.empty;
    });

    this.Then(/^the titles should have active status in banner service$/, function () {
        _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var statuses, response, data, _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, title, activeStatus, jsonData;

            return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                    switch (_context3.prev = _context3.next) {
                        case 0:
                            statuses = [];
                            _context3.next = 3;
                            return fetch(_this.endPoint);

                        case 3:
                            response = _context3.sent;

                            (0, _chai.expect)(response.status).to.be.eql(200);
                            _context3.next = 7;
                            return response.json();

                        case 7:
                            data = _context3.sent;
                            _iteratorNormalCompletion2 = true;
                            _didIteratorError2 = false;
                            _iteratorError2 = undefined;
                            _context3.prev = 11;


                            for (_iterator2 = bannerTitles[Symbol.iterator](); !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                                title = _step2.value;
                                activeStatus = "$.data.featured[?(@.content.common.carousel.label==\"" + title + "\")].content.common.carousel.active";
                                jsonData = (0, _advancedJsonPath2.default)(data, activeStatus);

                                statuses.push(jsonData);
                            }
                            _context3.next = 19;
                            break;

                        case 15:
                            _context3.prev = 15;
                            _context3.t0 = _context3['catch'](11);
                            _didIteratorError2 = true;
                            _iteratorError2 = _context3.t0;

                        case 19:
                            _context3.prev = 19;
                            _context3.prev = 20;

                            if (!_iteratorNormalCompletion2 && _iterator2.return) {
                                _iterator2.return();
                            }

                        case 22:
                            _context3.prev = 22;

                            if (!_didIteratorError2) {
                                _context3.next = 25;
                                break;
                            }

                            throw _iteratorError2;

                        case 25:
                            return _context3.finish(22);

                        case 26:
                            return _context3.finish(19);

                        case 27:
                            (0, _chai.expect)(statuses).to.have.length.above(0);

                            if (statuses.length > 1) {
                                (0, _chai.expect)(statuses).contains(true);
                            } else if (statuses.length == 1) {
                                (0, _chai.expect)(statuses).to.be.eql(true);
                            }

                        case 29:
                        case 'end':
                            return _context3.stop();
                    }
                }
            }, _callee3, _this, [[11, 15, 19, 27], [20,, 22, 26]]);
        }));
    });

    this.Given(/^I am in William Hill Sportsbook TopGames/, function () {
        var testurl = _config2.default.get(process.env.npm_config_env + '.test_url');
        browser.url(testurl + '#top-games');
    });

    this.When(/^I go to the create a x-sell banner on top games$/, function () {

        var ceiUrl = _config2.default.get(process.env.npm_config_env + '.squizCrossSellBannersNav_url');
        browser.url(ceiUrl);
    });

    this.When(/^I go to the top games configuration page$/, function () {

        var ceiUrl = _config2.default.get(process.env.npm_config_env + '.squizTopGames_url');
        browser.url(ceiUrl);
    });

    this.When(/^the top games overlay button should be present$/, function () {
        _banners2.default.sqzTopGamesOverlayBtn.waitForVisible(6500);
        return (0, _chai.expect)(_banners2.default.sqzTopGamesOverlayBtn.isVisible()).to.be.eql(true);
    });

    this.When(/^the banners attributes for promos should match the banner service$/, function () {
        browser.waitForVisible('.topgames__banner .banners-container .segmentation.forWeb', 8000);
        var _iteratorNormalCompletion3 = true;
        var _didIteratorError3 = false;
        var _iteratorError3 = undefined;

        try {
            for (var _iterator3 = _banners2.default.bannerLinks[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                var link = _step3.value;

                link.click();
                var _iteratorNormalCompletion4 = true;
                var _didIteratorError4 = false;
                var _iteratorError4 = undefined;

                try {
                    for (var _iterator4 = _banners2.default.bannersForWeb[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                        var banner = _step4.value;

                        mobileUrls.push(banner.getAttribute('mobile-url'));
                        desktopUrls.push(banner.getAttribute('desktop-url'));
                    }
                } catch (err) {
                    _didIteratorError4 = true;
                    _iteratorError4 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion4 && _iterator4.return) {
                            _iterator4.return();
                        }
                    } finally {
                        if (_didIteratorError4) {
                            throw _iteratorError4;
                        }
                    }
                }

                mobileUrls = mobileUrls.filter(function (item, index, inputArray) {
                    return inputArray.indexOf(item) === index;
                });
                desktopUrls = desktopUrls.filter(function (item, index, inputArray) {
                    return inputArray.indexOf(item) === index;
                });
            }

            //
            // for (let banner of bannerPage.bannersForNative) {
            //     nativeMobileUrls.push(banner.getAttribute('mobile-url'));
            //     nativeDesktopUrls.push(banner.getAttribute('desktop-url'));
            // }
            // nativeMobileUrls = nativeMobileUrls.filter(function (item, index, inputArray) {
            //     return inputArray.indexOf(item) === index;
            // });
            // nativeDesktopUrls = nativeDesktopUrls.filter(function (item, index, inputArray) {
            //     return inputArray.indexOf(item) === index;
            // });
            //
            //
            // for (let mobileUrl of mobileUrls) {
            //
            //     expect(ceiMobileUrls).to.include(mobileUrl);
            // }
            // for (let desktopUrl of desktopUrls) {
            //
            //     expect(ceiDesktopUrls).to.include(desktopUrl);
            // }
            //
            // for (let mobileUrl of nativeMobileUrls) {
            //
            //     expect(ceiMobileUrls).to.include(mobileUrl);
            // }
            // for (let desktopUrl of nativeDesktopUrls) {
            //
            //     expect(ceiDesktopUrls).to.include(desktopUrl);
            // }
        } catch (err) {
            _didIteratorError3 = true;
            _iteratorError3 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion3 && _iterator3.return) {
                    _iterator3.return();
                }
            } finally {
                if (_didIteratorError3) {
                    throw _iteratorError3;
                }
            }
        }
    });

    this.Then(/^the banner is displayed$/, function () {
        _banners2.default.bannersCarousels[0].waitForVisible();
        var _iteratorNormalCompletion5 = true;
        var _didIteratorError5 = false;
        var _iteratorError5 = undefined;

        try {
            for (var _iterator5 = _banners2.default.bannersCarousels[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
                var obj = _step5.value;

                (0, _chai.expect)(obj).isVisible;
            }
        } catch (err) {
            _didIteratorError5 = true;
            _iteratorError5 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion5 && _iterator5.return) {
                    _iterator5.return();
                }
            } finally {
                if (_didIteratorError5) {
                    throw _iteratorError5;
                }
            }
        }

        (0, _chai.expect)(_banners2.default.bannersCarousels).to.have.length.above(0);
    });
};
//# sourceMappingURL=banners_steps.js.map