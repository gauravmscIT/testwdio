'use strict';

var _chai = require('chai');

var _search = require('../../pageobjects/search.page');

var _search2 = _interopRequireDefault(_search);

var _checkProperty = require('../../support/checkProperty');

var _checkProperty2 = _interopRequireDefault(_checkProperty);

var _checkClass = require('../../support/checkClass');

var _checkClass2 = _interopRequireDefault(_checkClass);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var config = require('config');

module.exports = function () {

    this.When(/^I search for the keyword "([^"]*)"$/, function (keyword) {
        return _search2.default.doSearch(keyword);
    });

    this.When(/^I search for the keyword "([^"]*)" on mobile$/, function (keyword) {
        return _search2.default.doSearchOnMobile(keyword);
    });

    this.When(/^I search for another keyword "([^"]*)"$/, function (keyword) {
        return _search2.default.doAnotherSearch(keyword);
    });

    this.Then(/^I should get valid result$/, function () {
        _search2.default.searchEventResult.waitForVisible();
        return (0, _chai.expect)(_search2.default.searchEventResult.isVisible()).to.be.eql(true);
    });

    this.When(/^I search for the invalid keyword "([^"]*)"$/, function (keyword) {
        return _search2.default.doNegativeSearch(keyword);
    });

    this.Then(/^I should not see any results/, function () {
        return _search2.default.verifyNoSearchResults();
    });

    this.Then(/^I should not see any events$/, function () {
        return _search2.default.verifyNoSearchEvents();
    });

    this.When(/^I use recent search for the keyword "([^"]*)"$/, function (keyword) {
        _search2.default.openSearch();
        return _search2.default.useRecentSearch(keyword);
    });

    this.Then(/^I should be able to get "([^"]*)" as recent search$/, function (keyword) {
        _search2.default.openSearch();
        (0, _chai.expect)(_search2.default.searchResults.getText()).to.be.eql(keyword);
        _search2.default.searchResults.click();
        _search2.default.searchEventResult.waitForVisible();
        return (0, _chai.expect)(_search2.default.searchEventResult.isVisible()).to.be.eql(true);
    });

    this.Then(/^I add the following search items$/, function (table) {
        _search2.default.openSearch();
        return table.raw().forEach(function (item) {
            _search2.default.addMaxSearchItems(item);
            _search2.default.scroll();
        });
    });

    this.Then(/^only (\d+) recent searches should be present$/, function (int) {
        _search2.default.verifyMaxRecentSearchesAreNotZero();
        return _search2.default.verifyMaxRecentSearches(int);
    });

    this.Then(/^the top recent search is the last scrolled "([^"]*)"$/, function (keyword) {
        _search2.default.topRecentSearch.waitForVisible();
        return (0, _chai.expect)(_search2.default.topRecentSearch.getText()).to.be.eql(keyword);
    });

    this.Then(/^I select an odd$/, function () {
        return _search2.default.selectFirstOdd();
    });

    this.Then(/^the selection should be added to betslip$/, function () {
        _search2.default.addTobetSlipConfirmation.waitForVisible();
        return (0, _chai.expect)(_search2.default.addTobetSlipConfirmation.isVisible()).to.be.eql(true);
    });

    this.Then(/^I remove the added odd$/, function () {
        return _search2.default.selectFirstOdd();
    });

    this.Then(/^the selection should be removed from betslip$/, function () {
        _search2.default.removeFromBetSlipConfirmation.waitForVisible();
        return (0, _chai.expect)(_search2.default.removeFromBetSlipConfirmation.isVisible()).to.be.eql(true);
    });

    this.When(/^I open the search$/, function () {
        var testurl = config.get(process.env.npm_config_env + '.test_url');
        browser.url(testurl);
        return _search2.default.waitForOverlayToDisappear();
        _search2.default.openSearch();
    });

    this.When(/^I visit "([^"]*)"$/, function (url) {
        browser.url(url);
    });

    this.Then(/^the search overlay should be loaded$/, function () {
        _search2.default.searchField.waitForVisible();
        return (0, _chai.expect)(_search2.default.searchField.isVisible()).to.be.eql(true);
    });
};
//# sourceMappingURL=search_steps.js.map