'use strict';

var _chai = require('chai');

var _checkProperty = require('../../support/checkProperty');

var _checkProperty2 = _interopRequireDefault(_checkProperty);

var _checkClass = require('../../support/checkClass');

var _checkClass2 = _interopRequireDefault(_checkClass);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var config = require('config');

module.exports = function () {

    this.Then(/^I expect that the( css)* attribute "([^"]*)?" from element "([^"]*)?" is( not)* "([^"]*)?"$/, _checkProperty2.default);

    this.Then(/^I expect that element "([^"]*)?" (has|does not have) the class "([^"]*)?"$/, _checkClass2.default);
};
//# sourceMappingURL=searchElements_steps.js.map