'use strict';

var _chai = require('chai');

var _jackpot = require('../../pageobjects/jackpot.page');

var _jackpot2 = _interopRequireDefault(_jackpot);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var endPoint = void 0;
var config = require('config');

var JSONPath = require('advanced-json-path');

var tierNames = [];
var tierAmounts = [];
var gamesNames = [];

var tgoTierNames = [];
var tgoTierAmounts = [];
var tgoGamesNames = [];

var serviceTierNames = [];
var serviceTierAmounts = [];
var serviceGamesNames = [];

var notificationMessages = [];
var frontEndNotificationMessages = [];

var expectedTierNames = ["Vegas Jackpot", "Royal Jackpot", "Major Jackpot", "Mega Jackpot", "Super Jackpot"];

module.exports = function () {
    var _this = this;

    this.Given(/^I obtain vegas names from webservice$/, _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
        var response, data, names;
        return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
                        _this.endPoint = config.get(process.env.npm_config_env + '.jackpots_list');
                        _context.next = 4;
                        return fetch(_this.endPoint);

                    case 4:
                        response = _context.sent;

                        (0, _chai.expect)(response.status).to.be.eql(200);
                        _context.next = 8;
                        return response.json();

                    case 8:
                        data = _context.sent;
                        names = "$.jackpots[0].tiers[?(@.sort>=0)].name";

                        serviceTierNames = JSONPath(data, names);

                    case 11:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, _this);
    })));

    this.Given(/^I obtain vegas amounts from webservice$/, _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
        var response, data, titles;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
                        _this.endPoint = config.get(process.env.npm_config_env + '.jackpots_data');
                        _context2.next = 4;
                        return fetch(_this.endPoint);

                    case 4:
                        response = _context2.sent;

                        (0, _chai.expect)(response.status).to.be.eql(200);
                        _context2.next = 8;
                        return response.json();

                    case 8:
                        data = _context2.sent;
                        titles = "$.data.vegas[0].tiers[?(@.level>=0)].prize";

                        serviceTierAmounts = JSONPath(data, titles);
                        serviceTierAmounts = serviceTierAmounts.reverse();

                    case 12:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, _this);
    })));

    this.Given(/^I obtain notification messages from webservice$/, _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
        var response, data, messages;
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
            while (1) {
                switch (_context3.prev = _context3.next) {
                    case 0:
                        process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
                        _this.endPoint = config.get(process.env.npm_config_env + '.jackpots_data');
                        _context3.next = 4;
                        return fetch(_this.endPoint);

                    case 4:
                        response = _context3.sent;

                        (0, _chai.expect)(response.status).to.be.eql(200);
                        _context3.next = 8;
                        return response.json();

                    case 8:
                        data = _context3.sent;
                        messages = "$.data.vegas[0].messages[?(@.name=='Vegas Millions Jackpot')].message";

                        notificationMessages = JSONPath(data, messages);

                    case 11:
                    case 'end':
                        return _context3.stop();
                }
            }
        }, _callee3, _this);
    })));

    this.Then(/^the notification messages should not include non utf-8 chars$/, _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
        var _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, msg;

        return regeneratorRuntime.wrap(function _callee4$(_context4) {
            while (1) {
                switch (_context4.prev = _context4.next) {
                    case 0:
                        _iteratorNormalCompletion = true;
                        _didIteratorError = false;
                        _iteratorError = undefined;
                        _context4.prev = 3;


                        for (_iterator = notificationMessages[Symbol.iterator](); !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                            msg = _step.value;

                            (0, _chai.expect)(/[À-ú]/.test(msg)).to.be.eql(false);
                            (0, _chai.expect)(/\uFFFD/g.test(msg)).to.be.eql(false);
                        }

                        _context4.next = 11;
                        break;

                    case 7:
                        _context4.prev = 7;
                        _context4.t0 = _context4['catch'](3);
                        _didIteratorError = true;
                        _iteratorError = _context4.t0;

                    case 11:
                        _context4.prev = 11;
                        _context4.prev = 12;

                        if (!_iteratorNormalCompletion && _iterator.return) {
                            _iterator.return();
                        }

                    case 14:
                        _context4.prev = 14;

                        if (!_didIteratorError) {
                            _context4.next = 17;
                            break;
                        }

                        throw _iteratorError;

                    case 17:
                        return _context4.finish(14);

                    case 18:
                        return _context4.finish(11);

                    case 19:
                    case 'end':
                        return _context4.stop();
                }
            }
        }, _callee4, _this, [[3, 7, 11, 19], [12,, 14, 18]]);
    })));

    this.Then(/^the front end notification messages should not include non utf-8 chars$/, _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
        var _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, msg;

        return regeneratorRuntime.wrap(function _callee5$(_context5) {
            while (1) {
                switch (_context5.prev = _context5.next) {
                    case 0:
                        _iteratorNormalCompletion2 = true;
                        _didIteratorError2 = false;
                        _iteratorError2 = undefined;
                        _context5.prev = 3;


                        for (_iterator2 = frontEndNotificationMessages[Symbol.iterator](); !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                            msg = _step2.value;

                            (0, _chai.expect)(/[À-ú]/.test(msg)).to.be.eql(false);
                            (0, _chai.expect)(/\uFFFD/g.test(msg)).to.be.eql(false);
                        }

                        _context5.next = 11;
                        break;

                    case 7:
                        _context5.prev = 7;
                        _context5.t0 = _context5['catch'](3);
                        _didIteratorError2 = true;
                        _iteratorError2 = _context5.t0;

                    case 11:
                        _context5.prev = 11;
                        _context5.prev = 12;

                        if (!_iteratorNormalCompletion2 && _iterator2.return) {
                            _iterator2.return();
                        }

                    case 14:
                        _context5.prev = 14;

                        if (!_didIteratorError2) {
                            _context5.next = 17;
                            break;
                        }

                        throw _iteratorError2;

                    case 17:
                        return _context5.finish(14);

                    case 18:
                        return _context5.finish(11);

                    case 19:
                    case 'end':
                        return _context5.stop();
                }
            }
        }, _callee5, _this, [[3, 7, 11, 19], [12,, 14, 18]]);
    })));

    this.Given(/^I obtain vegas jackpot tier names and amounts from frontend$/, function () {

        var testurl = config.get(process.env.npm_config_env + '.vegas_url');
        browser.url(testurl);
        _jackpot2.default.jackpotTierNames[0].waitForVisible();
        _jackpot2.default.jackpotTierAmts[0].waitForVisible();
        _jackpot2.default.jackpotGamesTitles[0].waitForVisible();

        tierNames = _jackpot2.default.jackpotTierNames.map(function (tierName) {
            return tierName.getText();
        });
        tierAmounts = _jackpot2.default.jackpotTierAmts.map(function (tierAmt) {
            return tierAmt.getText();
        });
        gamesNames = _jackpot2.default.jackpotGamesTitles.map(function (game) {
            return game.getText();
        });
    });

    this.Given(/^I am on vegas page$/, function () {
        var testurl = config.get(process.env.npm_config_env + '.vegas_url');
        browser.url(testurl);
    });

    this.Given(/^the jackpot component should have correct child elements/, function () {

        (0, _chai.expect)($('.millions__logo').$('..').getAttribute('class')).to.be.eql(_jackpot2.default.mainJackpotComponent.getAttribute('class'));
        (0, _chai.expect)($('.millions__desc').$('..').getAttribute('class')).to.be.eql(_jackpot2.default.mainJackpotComponent.getAttribute('class'));
        (0, _chai.expect)($('.millions__content').$('..').getAttribute('class')).to.be.eql(_jackpot2.default.mainJackpotComponent.getAttribute('class'));
        (0, _chai.expect)(_jackpot2.default.jackpotCarouselComponent.isExisting()).to.be.eql(true);
    });

    this.Given(/^the tgo jackpot component should have correct child elements/, function () {

        (0, _chai.expect)($('.millions__logo').$('..').getAttribute('class')).to.be.eql(_jackpot2.default.mainTgoJackpotComponent.getAttribute('class'));
        (0, _chai.expect)($('.millions__desc').$('..').getAttribute('class')).to.be.eql(_jackpot2.default.mainTgoJackpotComponent.getAttribute('class'));
        (0, _chai.expect)($('.millions__content').$('..').getAttribute('class')).to.be.eql(_jackpot2.default.mainTgoJackpotComponent.getAttribute('class'));
        (0, _chai.expect)(_jackpot2.default.tgoJackpotCarouselComponent.isExisting()).to.be.eql(true);
    });

    this.Then(/^the jackpot container amounts should match vegas jackpot amounts$/, function () {

        _jackpot2.default.tgoTierAmts[0].waitForVisible();
        tgoTierAmounts = _jackpot2.default.tgoTierAmts.map(function (tierAmt) {
            return tierAmt.getText();
        });

        for (var i = 0; i < tierAmounts.length; i++) {
            tierAmounts[i] = tierAmounts[i].replace(/\,/g, '');
            tierAmounts[i] = Number(tierAmounts[i]);

            tgoTierAmounts[i] = tgoTierAmounts[i].replace(/\,/g, '');
            tgoTierAmounts[i] = Number(tgoTierAmounts[i]);

            (0, _chai.expect)(tierAmounts[i]).to.be.closeTo(tgoTierAmounts[i], 50);
        }
        (0, _chai.expect)(tierAmounts.length).to.be.eql(tgoTierAmounts.length);
    });

    this.Then(/^the jackpot container tier amounts should match the webservice$/, _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
        var i;
        return regeneratorRuntime.wrap(function _callee6$(_context6) {
            while (1) {
                switch (_context6.prev = _context6.next) {
                    case 0:
                        tgoTierAmounts = _jackpot2.default.tgoTierAmts.map(function (tierAmt) {
                            return tierAmt.getText().replace(/\,/g, '');
                        });
                        for (i = 0; i < serviceTierAmounts.length; i++) {
                            tgoTierAmounts[i] = Number(tgoTierAmounts[i]);
                            serviceTierAmounts[i] = Number(serviceTierAmounts[i]);
                            (0, _chai.expect)(tgoTierAmounts[i]).to.be.closeTo(serviceTierAmounts[i], 200);
                        }

                        (0, _chai.expect)(tgoTierAmounts.length).to.be.eql(serviceTierAmounts.length);

                    case 3:
                    case 'end':
                        return _context6.stop();
                }
            }
        }, _callee6, _this);
    })));

    this.Then(/^the jackpot container tier names should match vegas jackpot tier names$/, function () {
        _jackpot2.default.tgoTierNames[0].waitForVisible();
        tgoTierNames = _jackpot2.default.tgoTierNames.map(function (tierName) {
            return tierName.getText();
        });
        var _iteratorNormalCompletion3 = true;
        var _didIteratorError3 = false;
        var _iteratorError3 = undefined;

        try {
            for (var _iterator3 = expectedTierNames[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                var name = _step3.value;

                (0, _chai.expect)(tgoTierNames).to.include(name);
            }
        } catch (err) {
            _didIteratorError3 = true;
            _iteratorError3 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion3 && _iterator3.return) {
                    _iterator3.return();
                }
            } finally {
                if (_didIteratorError3) {
                    throw _iteratorError3;
                }
            }
        }

        (0, _chai.expect)(expectedTierNames.length).to.be.eql(tgoTierNames.length);
    });

    this.Then(/^the jackpot container tier names should match the webservice$/, _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
        var _iteratorNormalCompletion4, _didIteratorError4, _iteratorError4, _iterator4, _step4, name, _iteratorNormalCompletion5, _didIteratorError5, _iteratorError5, _iterator5, _step5, _name;

        return regeneratorRuntime.wrap(function _callee7$(_context7) {
            while (1) {
                switch (_context7.prev = _context7.next) {
                    case 0:

                        (0, _chai.expect)(serviceTierNames.length).to.be.eql(5);

                        _iteratorNormalCompletion4 = true;
                        _didIteratorError4 = false;
                        _iteratorError4 = undefined;
                        _context7.prev = 4;
                        for (_iterator4 = expectedTierNames[Symbol.iterator](); !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                            name = _step4.value;

                            (0, _chai.expect)(serviceTierNames).to.include(name);
                        }
                        _context7.next = 12;
                        break;

                    case 8:
                        _context7.prev = 8;
                        _context7.t0 = _context7['catch'](4);
                        _didIteratorError4 = true;
                        _iteratorError4 = _context7.t0;

                    case 12:
                        _context7.prev = 12;
                        _context7.prev = 13;

                        if (!_iteratorNormalCompletion4 && _iterator4.return) {
                            _iterator4.return();
                        }

                    case 15:
                        _context7.prev = 15;

                        if (!_didIteratorError4) {
                            _context7.next = 18;
                            break;
                        }

                        throw _iteratorError4;

                    case 18:
                        return _context7.finish(15);

                    case 19:
                        return _context7.finish(12);

                    case 20:
                        (0, _chai.expect)(expectedTierNames.length).to.be.eql(serviceTierNames.length);

                        _iteratorNormalCompletion5 = true;
                        _didIteratorError5 = false;
                        _iteratorError5 = undefined;
                        _context7.prev = 24;
                        for (_iterator5 = tgoTierNames[Symbol.iterator](); !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
                            _name = _step5.value;

                            (0, _chai.expect)(serviceTierNames).to.include(_name);
                        }
                        _context7.next = 32;
                        break;

                    case 28:
                        _context7.prev = 28;
                        _context7.t1 = _context7['catch'](24);
                        _didIteratorError5 = true;
                        _iteratorError5 = _context7.t1;

                    case 32:
                        _context7.prev = 32;
                        _context7.prev = 33;

                        if (!_iteratorNormalCompletion5 && _iterator5.return) {
                            _iterator5.return();
                        }

                    case 35:
                        _context7.prev = 35;

                        if (!_didIteratorError5) {
                            _context7.next = 38;
                            break;
                        }

                        throw _iteratorError5;

                    case 38:
                        return _context7.finish(35);

                    case 39:
                        return _context7.finish(32);

                    case 40:
                        (0, _chai.expect)(tgoTierNames.length).to.be.eql(serviceTierNames.length);

                    case 41:
                    case 'end':
                        return _context7.stop();
                }
            }
        }, _callee7, _this, [[4, 8, 12, 20], [13,, 15, 19], [24, 28, 32, 40], [33,, 35, 39]]);
    })));

    this.Then(/^the jackpot container games titles should mach vegas jackpot games titles$/, function () {

        _jackpot2.default.tgoGamesTitles[0].waitForVisible();
        tgoGamesNames = _jackpot2.default.tgo.map(function (gameTitle) {
            return gameTitle.getText();
        });

        for (var i = 0; i < gamesNames.length; i++) {
            (0, _chai.expect)(tgoGamesNames[i]).to.be.eql(gamesNames[i]);
        }
        (0, _chai.expect)(tgoGamesNames.length).to.be.eql(gamesNames.length);
    });

    this.Then(/^the jackpot notification items should be present$/, function () {

        _jackpot2.default.jackpotNotificationItems[0].waitForVisible();

        frontEndNotificationMessages = _jackpot2.default.jackpotNotificationItems.filter(function (item) {
            return item.getText() !== '';
        }).map(function (item) {
            return item.getText();
        });

        (0, _chai.expect)(frontEndNotificationMessages).to.have.length.above(0);

        var _iteratorNormalCompletion6 = true;
        var _didIteratorError6 = false;
        var _iteratorError6 = undefined;

        try {
            for (var _iterator6 = frontEndNotificationMessages[Symbol.iterator](), _step6; !(_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done); _iteratorNormalCompletion6 = true) {
                var msg = _step6.value;

                (0, _chai.expect)(notificationMessages).to.include(msg);
            }
        } catch (err) {
            _didIteratorError6 = true;
            _iteratorError6 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion6 && _iterator6.return) {
                    _iterator6.return();
                }
            } finally {
                if (_didIteratorError6) {
                    throw _iteratorError6;
                }
            }
        }
    });

    this.Then(/^the vegas jackpot amounts should be going up$/, function () {

        var newTierAmounts = [];

        _jackpot2.default.jackpotTierAmts[0].waitForVisible();

        tierAmounts = _jackpot2.default.jackpotTierAmts.map(function (tierAmt) {
            return tierAmt.getText();
        });
        browser.pause(3000);
        //static wait to check if the amounts will go up
        newTierAmounts = _jackpot2.default.jackpotTierAmts.map(function (tierAmt) {
            return tierAmt.getText();
        });

        for (var i = 0; i < tierAmounts.length; i++) {
            (0, _chai.expect)(newTierAmounts[i]).to.be.greaterThan(tierAmounts[i]);
        }
    });

    this.Then(/^the tgo jackpot amounts should be going up$/, function () {

        var newTierAmounts = [];

        _jackpot2.default.tgoTierAmts[0].waitForVisible();

        tierAmounts = _jackpot2.default.tgoTierAmts.map(function (tierAmt) {
            return tierAmt.getText();
        });
        browser.pause(3000);
        //static wait to check if the amounts will go up
        newTierAmounts = _jackpot2.default.tgoTierAmts.map(function (tierAmt) {
            return tierAmt.getText();
        });

        for (var i = 0; i < tierAmounts.length; i++) {
            (0, _chai.expect)(newTierAmounts[i]).to.be.greaterThan(tierAmounts[i]);
        }
    });
};
//# sourceMappingURL=jackpot_steps.js.map