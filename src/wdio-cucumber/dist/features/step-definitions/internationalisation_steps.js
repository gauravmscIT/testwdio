'use strict';

var _chai = require('chai');

var _internationalisation = require('../../pageobjects/internationalisation.page');

var _internationalisation2 = _interopRequireDefault(_internationalisation);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var config = require('config');

var JSONPath = require('advanced-json-path');

var endPoint = void 0;

var extracted = function extracted(client, url) {
    client.get(url, function (data, response) {
        return response.statusCode;
    });
};
module.exports = function () {
    var _this = this;

    this.When(/^I am in William Hill Sportsbook for language "([^"]*)"$/, function (field) {
        return _this.endPoint = _internationalisation2.default.buildSportsBookPageWithLang(field);
    });

    this.Then(/^the page should return the successful response status code$/, _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
        var response;
        return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.next = 2;
                        return fetch(_this.endPoint);

                    case 2:
                        response = _context.sent;

                        (0, _chai.expect)(response.status).to.be.eql(200);

                    case 4:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, _this);
    })));

    this.When(/^I hit top games end point json for "([^"]*)"$/, function (field) {
        return _this.endPoint = _internationalisation2.default.buildSquizTGOWithLang(field);
    });

    this.When(/^I hit notification end point json for "([^"]*)"$/, function (field) {
        return _this.endPoint = _internationalisation2.default.buildNotificationEndpointWithLang(field);
    });

    this.When(/^I open William Hill Sportsbook homepage for language "([^"]*)"$/, function (field) {
        var testurl = config.get(process.env.npm_config_env + '.test_url');
        testurl = testurl.replace("en-gb", field);
        browser.url(testurl);
    });

    this.When(/^the page should be loaded for lang "([^"]*)"$/, function (field) {
        (0, _chai.expect)(browser.getAttribute('html', 'lang')).to.be.eql(field);
    });

    this.When(/^I hit GamesLobby Fragment for "([^"]*)"$/, function (field) {
        return _this.endPoint = _internationalisation2.default.buildGamesLobbyFragmentWithLang(field);
    });
};
//# sourceMappingURL=internationalisation_steps.js.map