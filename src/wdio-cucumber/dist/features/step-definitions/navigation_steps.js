'use strict';

var _chai = require('chai');

var _home = require('../../pageobjects/home.page');

var _home2 = _interopRequireDefault(_home);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var config = require('config');
var testurl = void 0;
var href_value = void 0;
var expectedMenus = ["/", "/what-we-do.html", "/our-work.html", "/resources.html", "/about.html", "/newsroom.html", "/contact.html"];

module.exports = function () {
    var _this = this;

    this.Given(/^I am in YLD homepage$/, _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
        return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        testurl = config.get(process.env.npm_config_env + '.test_url');
                        browser.url(testurl);

                    case 2:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, _this);
    })));

    this.Then(/^the section "([^"]*)" should be present$/, function (section) {
        var sectionNames = _home2.default.sectionTitles.map(function (sectionName) {
            return sectionName.getText();
        });
        (0, _chai.expect)(sectionNames).to.include(section);
    });

    this.Then(/^I obtain the href value of "([^"]*)"$/, function (menu) {
        return href_value = _home2.default.navigationLinks.filter(function (menuName) {
            return menuName.getText() === menu;
        }).map(function (linkmenu) {
            return linkmenu.getAttribute('href');
        });
    });

    this.Then(/^the href value should be "([^"]*)"$/, function (value) {
        return (0, _chai.expect)(href_value.toString()).to.be.eql(testurl + value.toString());
    });

    this.When(/^I am in page "([^"]*)"$/, function (value) {
        return browser.url(value);
    });

    this.Then(/^I should be able to access all linked pages$/, _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
        var actualMenus, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, expectedMenu;

        return regeneratorRuntime.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        actualMenus = _home2.default.navigationLinks.map(function (linkmenu) {
                            return linkmenu.getAttribute('href');
                        });
                        _iteratorNormalCompletion = true;
                        _didIteratorError = false;
                        _iteratorError = undefined;
                        _context2.prev = 4;

                        for (_iterator = expectedMenus[Symbol.iterator](); !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                            expectedMenu = _step.value;

                            (0, _chai.expect)(actualMenus).to.include(testurl + expectedMenu.toString());
                        }
                        _context2.next = 12;
                        break;

                    case 8:
                        _context2.prev = 8;
                        _context2.t0 = _context2['catch'](4);
                        _didIteratorError = true;
                        _iteratorError = _context2.t0;

                    case 12:
                        _context2.prev = 12;
                        _context2.prev = 13;

                        if (!_iteratorNormalCompletion && _iterator.return) {
                            _iterator.return();
                        }

                    case 15:
                        _context2.prev = 15;

                        if (!_didIteratorError) {
                            _context2.next = 18;
                            break;
                        }

                        throw _iteratorError;

                    case 18:
                        return _context2.finish(15);

                    case 19:
                        return _context2.finish(12);

                    case 20:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, _this, [[4, 8, 12, 20], [13,, 15, 19]]);
    })));

    this.When(/^I am in yld blog page$/, _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
            while (1) {
                switch (_context3.prev = _context3.next) {
                    case 0:
                        testurl = config.get(process.env.npm_config_env + '.blog_url');
                        browser.url(testurl);

                    case 2:
                    case 'end':
                        return _context3.stop();
                }
            }
        }, _callee3, _this);
    })));

    this.When(/^I enter a valid email address to subscribe$/, _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
        return regeneratorRuntime.wrap(function _callee4$(_context4) {
            while (1) {
                switch (_context4.prev = _context4.next) {
                    case 0:
                        _home2.default.emailField.clearElement();
                        _home2.default.emailField.setValue(process.env.npm_config_email);
                        _home2.default.subscribeBtn.click();

                    case 3:
                    case 'end':
                        return _context4.stop();
                }
            }
        }, _callee4, _this);
    })));

    this.Then(/^the message "([^"]*)" should be displayed$/, function (msg) {
        _home2.default.confirmationMsg.waitForVisible();
        return (0, _chai.expect)(_home2.default.confirmationMsg.getText()).to.be.eql(msg);
    });

    this.Then(/^the email field should disappear$/, _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
        return regeneratorRuntime.wrap(function _callee5$(_context5) {
            while (1) {
                switch (_context5.prev = _context5.next) {
                    case 0:
                        (0, _chai.expect)(_home2.default.emailField.isVisible()).to.be.eql(false);

                    case 1:
                    case 'end':
                        return _context5.stop();
                }
            }
        }, _callee5, _this);
    })));
};
//# sourceMappingURL=navigation_steps.js.map