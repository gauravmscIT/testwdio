import {expect} from 'chai';
import Page from './page';

let config = require('config');

class homePage extends Page {
    get emailField() {
        return $("[name='email'][class='hs-input']");
    }

    get subscribeBtn() {
        return $("[value='Subscribe'][class='hs-button primary large']");
    }

    get confirmationMsg() {
        return $(".submitted-message");
    }

    get sectionTitles() {
        return $$(".mdl-typography--display-1.title");
    }

    get navigationLinks() {
        return $$("a.mdl-navigation__link");
    }
}

export default new homePage();
