import {expect} from 'chai';
import homepage from '../../pageobjects/home.page';

let config = require('config');
let testurl;
let href_value;
let expectedMenus = ["/", "/what-we-do.html", "/our-work.html", "/resources.html", "/about.html", "/newsroom.html", "/contact.html"];

module.exports = function () {

    this.Given(/^I am in YLD homepage$/, async () => {
        testurl = config.get(process.env.npm_config_env + '.test_url');
        browser.url(testurl);
    });

    this.Then(/^the section "([^"]*)" should be present$/, section => {
        let sectionNames = homepage.sectionTitles.map(sectionName => sectionName.getText());
        expect(sectionNames).to.include(section);
    });

    this.Then(/^I obtain the href value of "([^"]*)"$/, menu => {
        return href_value = homepage.navigationLinks.filter(menuName => menuName.getText() === menu).map(linkmenu => linkmenu.getAttribute('href'));
    });

    this.Then(/^the href value should be "([^"]*)"$/, value => {
        return expect(href_value.toString()).to.be.eql(testurl + value.toString());
    });

    this.When(/^I am in page "([^"]*)"$/, value => {
        return browser.url(value);
    });

    this.Then(/^I should be able to access all linked pages$/, async () => {
        let actualMenus = homepage.navigationLinks.map(linkmenu => linkmenu.getAttribute('href'));
        for (let expectedMenu of expectedMenus) {
            expect(actualMenus).to.include(testurl + expectedMenu.toString());
        }
    });

    this.When(/^I am in yld blog page$/, async () => {
        testurl = config.get(process.env.npm_config_env + '.blog_url');
        browser.url(testurl);
    });

    this.When(/^I enter a valid email address to subscribe$/, async () => {
        homepage.emailField.clearElement();
        homepage.emailField.setValue(process.env.npm_config_email);
        homepage.subscribeBtn.click();
    });

    this.Then(/^the message "([^"]*)" should be displayed$/, msg => {
        homepage.confirmationMsg.waitForVisible();
        return expect(homepage.confirmationMsg.getText()).to.be.eql(msg);
    });

    this.Then(/^the email field should disappear$/, async () => {
        expect(homepage.emailField.isVisible()).to.be.eql(false);
    });
};
