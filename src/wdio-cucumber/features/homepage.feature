@e2e
Feature: HomePage
  As a user,
  I want to see the home page of the website,
  I want to be to be able to use the menu,
  I want to be able to subscribe to the YLD Newsletter

  Scenario Outline: User to be able to see the sections on homepage
    When I am in YLD homepage
    Then the section "<section>" should be present
    Examples: Section Names
      | section    |
      | What we do |
      | Customers  |

  Scenario Outline: Menus to be correctly linked
    When I am in YLD homepage
    And I obtain the href value of "<menu>"
    Then the href value should be "<value>"
    Examples: Menus and values
      | menu       | value            |
      | Home       | /                |
      | What we do | /what-we-do.html |
      | Our work   | /our-work.html   |
      | Resources  | /resources.html  |
      | About      | /about.html      |
      | Newsroom   | /newsroom.html   |
      | Contact    | /contact.html    |

  Scenario Outline: User to be able to visit other pages from all pages
    When I am in page "<page>"
    Then I should be able to access all linked pages
    Examples:
      | page             |
      | /                |
      | /what-we-do.html |
      | /our-work.html   |
      | /resources.html  |
      | /about.html      |
      | /newsroom.html   |
      | /contact.html    |

  Scenario: User to be able to subscribe to newsletter
    When I am in yld blog page
    And I enter a valid email address to subscribe
    Then the message "Thank you for subscribing to our blog updates." should be displayed
    And the email field should disappear

